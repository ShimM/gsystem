#pragma once

class CStream
{
public:
    CStream(VOID);
    ~CStream(VOID);
    CStream(BYTE *buf);

    BOOL SetBuffer(BYTE *buffer);
    int  GetLength(VOID);

    VOID Reserve(int n);    // 버퍼에 공간 예약. 맨 처음에만 쓰는 것이 좋을 듯
//    VOID Rewind(VOID);      // offset을 처음으로 되돌린다

//    int  PutSize(VOID);

    //SHORT   ReadProtocol(VOID);   // 굳이 이걸 사용하지 않는다
    BOOL    Write1(BYTE data);   // 고정된 크기의 1 byte를 버퍼에 기록
    BOOL    Write2(SHORT data);   // 고정된 크기의 2 byte를 버퍼에 기록

    BYTE    ReadByte(VOID);
    CHAR    ReadChar(VOID);
    SHORT   ReadShort(VOID);
    INT     ReadInt(VOID);
    INT64   ReadInt64(VOID);
    BOOL    ReadStr(char *data);

    BOOL    WriteByte(BYTE data);
    BOOL    WriteChar(CHAR data);
    BOOL    WriteShort(SHORT data);
    BOOL    WriteInt(INT data);
    BOOL    WriteInt64(INT64 data);
    BOOL    WriteStr(char *data);

private:
    int    ReadInt64(INT64 *pData);   // return result size
    BYTE   GetType() { return mBuffer[mOffset++]; }

private:
    BYTE *mBuffer;
    int   mOffset;
};

inline void CopyMemory1(VOID *dst, VOID *src)
{
    *(BYTE *)dst = *(BYTE *)src;
}

inline void CopyMemory2(VOID *dst, VOID *src)
{
    *(SHORT *)dst = *(SHORT *)src;
}

inline void CopyMemory4(VOID *dst, VOID *src)
{
    *(INT32 *)dst = *(INT32 *)src;
}

inline void CopyMemory8(VOID *dst, VOID *src)
{
    *(INT64 *)dst = *(INT64 *)src;
}