#pragma once

#include "Share.h"

int EncodeNumbers(OUT BYTE *buffer, SHORT protocol, CHAR c, INT i);
BOOL DecodeNumbers(IN BYTE *buffer, IN int length, OUT CHAR *d, OUT INT *j);

int EncodeLogin(OUT BYTE *buffer, SHORT protocol, char *id, char *passwd);
BOOL DecodeLogin(IN BYTE *buffer, IN int length, OUT char *id, OUT char *passwd);

int EncodeRoom(OUT BYTE *buffer, SHORT protocol);
BOOL DecodeRoom(IN BYTE *buffer, IN int length);

int EncodeUser(OUT BYTE *buffer, SHORT protocol);
BOOL DecodeUser(IN BYTE *buffer, IN int length);

int EncodeEnter(OUT BYTE *buffer, SHORT protocol, DWORD n);
BOOL DecodeEnter(IN BYTE *buffer, IN int length, OUT DWORD *n);

int EncodeChat(OUT BYTE *buffer, SHORT protocol, char *p);
BOOL DecodeChat(IN BYTE *buffer, IN int length, OUT char *p);