#include "stdafx.h"

// 프로토콜 공통 요소를 뽑으면 다음과 같다
// 아규먼트 이름은 덜 중요하지만, 사용자가 알아보기에 편하다
// 아래 두 가지 방식 중의 하나를 선택하면 될 듯?

// Numbers = { Char c, Int i }
// Login   = { String id, String passwd }

// JSON으로 나타내면 다음과 같이 될 것이다.
// { Numbers : { c : Char, i : Int }}
// { Login : { id : String, passwd : String }}


// 패킷 인코딩과 디코딩은 순서에 따라서 진행하되, msgpack의 방식을 사용하여, 인코딩과 디코딩이 짝이 맞는지 검사한다

int EncodeNumbers(OUT BYTE *buffer, SHORT protocol, CHAR c, INT i)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size
    stream.Write2(protocol);
    ////
    stream.WriteChar(c);
    stream.WriteInt(i);
    ////
    return stream.GetLength();
}

BOOL DecodeNumbers(IN BYTE *buffer, IN int length, OUT CHAR *d, OUT INT *j) // protocol은 항상 두 바이트로 처리한다. (그렇지 않으면 1~3 바이트를 차지한다)
{
    CStream stream(buffer);
    ////
    *d = stream.ReadChar();
    *j = stream.ReadInt();
    ////
    if (stream.GetLength() > length) {
        assert(!"DecodeNumbers");
        return FALSE;
    }
    return TRUE;
}

int EncodeLogin(OUT BYTE *buffer, SHORT protocol, char *id, char *passwd)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size
    stream.Write2(protocol);
    ////
    stream.WriteStr(id);
    stream.WriteStr(passwd);
    ////
    return stream.GetLength();
}

BOOL DecodeLogin(IN BYTE *buffer, IN int length, OUT char *id, OUT char *passwd)
{
    CStream stream(buffer);
    ////
    stream.ReadStr(id);
    stream.ReadStr(passwd);
    ////
    if (stream.GetLength() > length) {
        assert(!"DecodeLogin");
        return FALSE;
    }
    return TRUE;
}

int EncodeRoom(OUT BYTE *buffer, SHORT protocol)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size
    stream.Write2(protocol);
    ////
    ////
    return stream.GetLength();
}

BOOL DecodeRoom(IN BYTE *buffer, IN int length)
{
    CStream stream(buffer);
    ////
    ////
    return TRUE;
}

int EncodeUser(OUT BYTE *buffer, SHORT protocol)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size
    stream.Write2(protocol);

    return stream.GetLength();
}

BOOL DecodeUser(IN BYTE *buffer, IN int length)
{
    CStream stream(buffer);

    return TRUE;
}

int EncodeEnter(OUT BYTE *buffer, SHORT protocol, DWORD n)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size
    stream.Write2(protocol);
    ////
    stream.WriteInt(n);
    ////
    return stream.GetLength();
}

BOOL DecodeEnter(IN BYTE *buffer, IN int length, OUT DWORD *n)
{
    CStream stream(buffer);
    ////
    *n = stream.ReadInt();
    ////
    return TRUE;
}

int EncodeChat(OUT BYTE *buffer, SHORT protocol, char *p)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size
    stream.Write2(protocol);
    ////
    stream.WriteStr(p);
    ////
    return stream.GetLength();
}

BOOL DecodeChat(IN BYTE *buffer, IN int length, OUT char *p)
{
    CStream stream(buffer);
    ////
    stream.ReadStr(p);
    ////
    return TRUE;
}
