#include "stdafx.h"

CStream::CStream(VOID)
{
    mBuffer = NULL;
    mOffset = 0;
}

CStream::~CStream(VOID)
{
}

CStream::CStream(BYTE *buffer)
{
    mBuffer = buffer;
    mOffset = 0;
}

BOOL CStream::SetBuffer(BYTE *buffer)
{
    if (!buffer)
        return FALSE;

    mBuffer = buffer;
    mOffset = 0;

    return TRUE;
}

VOID CStream::Reserve(int n)
{
    mOffset += n;
}

//VOID CStream::Rewind(VOID)
//{
//    mOffset = 0;
//}

//int CStream::PutSize(VOID)
//{
//    int length = GetLength();
//    Rewind();    // 길이를 구한 다음 되돌림
//    WriteTwoByte(length);
//    return length;
//}

int CStream::GetLength(VOID)
{ 
    return mOffset; 
}

// MSGPACK FORMAT
// POSITIVE FIXINT  0XXXXXXX = 0x00~0xFF : 한바이트 양수는 한바이트로 표현
// NEGATIVE FIXINT  111YYYYY = -32 ~ -1  : 이 범위의 음수는 한 바이트로 표현
// INT 8, 16, 32, 64  : D0, D1, D2, D3
// UINT 8, 16, 32, 64 : CC, CD, CE, CF

// Read

//SHORT CStream::ReadProtocol(VOID)
//{
//    SHORT data;
//    CopyMemory2(&data, mBuffer + mOffset);
//    mOffset += sizeof(SHORT);
//    return data;
//}

// unsigned
BYTE CStream::ReadByte(VOID)
{
    INT64 data;
    int ret = ReadInt64(&data);
    if (ret == 1)
        return (BYTE)data;

    assert(!"ReadByte");
    return 0;
}

// signed

CHAR CStream::ReadChar(VOID)
{
    INT64 data;
    int ret = ReadInt64(&data);
    if (ret == 1)
        return (CHAR)data;

    assert(!"ReadChar");
    return 0;
}

SHORT CStream::ReadShort(VOID)
{
    INT64 data;
    int ret = ReadInt64(&data);
    if (ret && ret <= 2)
        return (SHORT)data;

    assert(!"ReadShort");
    return 0;
}

INT32 CStream::ReadInt(VOID)
{
    INT64 data;
    int ret = ReadInt64(&data);
    if (ret && ret <= 4)
        return (INT32)data;

    assert(!"ReadInt");
    return 0;
}

INT64 CStream::ReadInt64(VOID)
{
    INT64 data;
    if (ReadInt64(&data))
        return data;

    assert(!"ReadInt64");
    return 0;
}

int CStream::ReadInt64(OUT INT64 *pData)
{
    *pData = 0; // 꼭 지워놔야 함
    BYTE type = GetType();
    if ((char)type >= -32) {
        *pData = type;
        return 1;
    }
    switch (type) {
    case 0xCC:
    case 0xD0:
        CopyMemory1(pData, mBuffer + mOffset);
        mOffset += 1;
        return 1;
        break;

    case 0xD1:
        CopyMemory2(pData, mBuffer + mOffset);
        mOffset += 2;
        return 2;
        break;

    case 0xD2:
        CopyMemory4(pData, mBuffer + mOffset);
        mOffset += 4;
        return 4;
        break;

    case 0xD3:
        CopyMemory8(pData, mBuffer + mOffset);
        mOffset += 8;
        return 8;
        break;
   }
   assert(!"ReadInt64");
   return 0;
}


BOOL CStream::ReadStr(char *data)
{
    BYTE type = GetType();
    if (type != 0xD9)
        return FALSE;

    int len = GetType();
    CopyMemory(data, mBuffer + mOffset, len);
    mOffset += len;
    data[len] = '\0';

    return TRUE;
}

// write


BOOL CStream::Write2(SHORT data) // 무조건 2바이트 (protocol, etc)
{
    CopyMemory2(mBuffer + mOffset, &data);
    mOffset += sizeof(SHORT);
    return TRUE;
}

BOOL CStream::Write1(BYTE data)
{
    CopyMemory1(mBuffer + mOffset, &data);
    mOffset += 1;
    return TRUE;
}

#define SetType(type)    (mBuffer[mOffset++] = type)

// unsigned
BOOL CStream::WriteByte(BYTE data)
{
    if (((char)data) >= -32) {
        SetType(data);
        return TRUE;
    }
    SetType(0xCC);
    CopyMemory1(mBuffer + mOffset, &data);
    mOffset += sizeof(BYTE);
    return TRUE;
}

// signed
BOOL CStream::WriteChar(CHAR data)
{
    if (data >= -32) {
        SetType(data);
        return TRUE;
    }
    SetType(0xD0);
    CopyMemory1(mBuffer + mOffset, &data);
    mOffset += sizeof(CHAR);
    return TRUE;
}

BOOL CStream::WriteShort(SHORT data)
{
    if ((data & 0xFF00) == 0) {
        WriteChar((CHAR)data);
        return TRUE;
    }
    SetType(0xD1);
    CopyMemory2(mBuffer + mOffset, &data);
    mOffset += sizeof(SHORT);
    return TRUE;
}

BOOL CStream::WriteInt(INT data)
{
    if ((data & 0xFFFF0000) == 0) {
        WriteShort((SHORT)data);
        return TRUE;
    }
    SetType(0xD2);
    CopyMemory4(mBuffer + mOffset, &data);
    mOffset += sizeof(INT);
    return TRUE;
}

BOOL CStream::WriteInt64(INT64 data)
{
    if ((data & 0xFFFFFFFF00000000) == 0) {
        WriteInt((INT)data);
        return TRUE;
    }
    SetType(0xD3);
    CopyMemory8(mBuffer + mOffset, &data);
    mOffset += sizeof(INT);
    return TRUE;
}

BOOL CStream::WriteStr(char *pdata)
{
    SetType(0xD9);  // str8 포맷으로 저장 (대부분 경우 이것으로 충분할듯)
    int len = (int)strlen(pdata);
    Write1(len);
    CopyMemory(mBuffer + mOffset, pdata, len);
    mOffset += len;
    return TRUE;
}