#pragma once

#ifdef _MSC_VER

#ifdef _DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#ifndef _CONSOLE
#include <cstdlib>
#endif

#ifdef malloc
#undef malloc
#endif
#define malloc(s) (_malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__))

class CMemoryLeak
{
public:
    CMemoryLeak() {
        _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

        _CrtSetReportMode(_CRT_WARN, _CRTDBG_MODE_DEBUG);
        _CrtSetReportMode(_CRT_ERROR, _CRTDBG_MODE_DEBUG);
        _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_DEBUG);
    }
};

static CMemoryLeak sMemoryLeak; // cpp file마다 생성될 것 같지만, 기능은 잘 동작한다.
#endif // _DEBUG

#ifdef _DEBUG
// 이렇게 두 줄을 사용하여 leak가 발생한 위치를 파악할 수 있다
#define MYDEBUG_NEW   new( _NORMAL_BLOCK, __FILE__, __LINE__)   
#define new MYDEBUG_NEW
// Replace _NORMAL_BLOCK with _CLIENT_BLOCK if you want the allocations to be of _CLIENT_BLOCK type
//#else
//#define MYDEBUG_NEW
#endif // _DEBUG

#endif // MSC_VER


// "게임 프로그래머를 위한 C++" p.186의 아이디어:
// ReportMemoryLeaks : 두 북마크 사이에 발생한 메모리 할당 블록 중에 여전히 이용중인 것들에 대한 리포트

