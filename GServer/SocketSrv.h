﻿#pragma once

#define READ_BUFFER_LENGTH      4096
const DWORD WRITE_BUFFER_LENGTH = 8192;
const DWORD WRITE_BUFFER_MASK = WRITE_BUFFER_LENGTH - 1;

class CSocketSrv {
public:
    CSocketSrv();

    BOOL Startup(VOID);
    BOOL Cleanup(VOID);

    BOOL Bind(VOID);
    BOOL Listen(WORD port, INT backlog);
    BOOL Connect(PCWSTR address, WORD port);
    BOOL Accept(SOCKET listenSocket);

    SOCKET GetSocket(VOID);

protected:
    // mReadBuffer  <= mReadHead <= mReadTail <= mReadEnd
    CHAR    mReadBuffer[READ_BUFFER_LENGTH];
    DWORD   mReadEnd, mReadHead, mReadTail;

    // Read와 비슷한 구조
    CHAR    mWriteBuffer[WRITE_BUFFER_LENGTH];
    DWORD   mWriteEnd, mWriteHead, mWriteTail;   

    SOCKET  mSocket;

    OVERLAPPED_EX   mAcceptOverlapped;
    OVERLAPPED_EX   mReadOverlapped;
    OVERLAPPED_EX   mWriteOverlapped;
};
