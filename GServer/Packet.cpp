﻿#include "stdafx.h"
#include "protocol.h"


// 기본적인 Test용 0번 패킷이다
static BOOL ProcSum(BYTE *body, WORD length, CSocket *socket)
{
    BYTE buf[4096] = { 0, 0, 0, };  // 임시 버퍼 스택에 할당. 이것을 삭제하는 별도 작업은 없다
// 테스트를 위해 일단 놔두자. 아주 나중에 지워도 늦지 않다.
//#ifdef _DEBUG 
    // 패킷을 읽어서 처리하는 아주 간단한 예.
    // 일단 여기서 하는 일은 패킷 바디의 바이트들의 합을 보내 주는 것이다. (일종의 테스트 셋?)
    if (length == 2) {  // body 길이가 2인 것만 취급한다
        BYTE sum = 0;
        //for (int i = 0; i < length; i++)
        //    sum += body[i];
        BYTE x, y;
        CStream stream(body);
        x = stream.ReadByte();
        y = stream.ReadByte();
        sum = x + y;

        //PutPacket을 위한 buf[0]과 buf[1]은 reserve해 놓으면 되며, PutPacket이 첫번째 인자로 채우므로, 채울 필요 없다. 
        buf[3] = sum;

//        socket->PutPacket(512, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.
        socket->PutPacket(1000, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.
        return TRUE;
    }
//#endif

    return TRUE;
}

#if 0

// 패킷 인코딩과 디코딩은 순서에 따라서 진행하되, msgpack의 방식을 사용하여, 인코딩과 디코딩이 짝이 맞는지 검사한다

int EncodeNumbers(OUT BYTE *buffer, SHORT protocol, CHAR c, INT i)
{
    CStream stream(buffer);
    stream.Reserve(2); // for size

    stream.WriteProtocol(protocol);
    stream.WriteChar(c);
    stream.WriteInt(i);

    //return stream.PutSize();
    return stream.GetLength();
}

BOOL DecodeNumbers(IN BYTE *buffer, IN int length, OUT CHAR *d, OUT INT *j) // protocol은 항상 두 바이트로 예외처리하고 있는데, 그렇지 않으면 1~3 바이트를 차지할 것이다. 어느 것이 나을까?
{
    CStream stream(buffer);

    *d = stream.ReadChar();
    *j = stream.ReadInt();

    if (stream.GetLength() > length) {
        assert(!"DecodeNumbers");
        return FALSE;
    }

    return TRUE;
}

#endif 

static BOOL ProcNumbers(BYTE *body, WORD length, CSocket *socket)
{
    // protocol buffer, thrift, avro, msgpack, etc.
    // 패킷을 디코딩한 다음에 내부에서 뭔가 처리하고,
    // 그 다음에는 클라이언트에 다시 패킷을 보내야겠지?

    // 이번 것은 입력으로 값과 갯수를 입력 받아서, 해당 값을 갯수만큼 갖고 있는 패킷을 만들어서 보내 준다.
    // 값은 한 바이트. 길이는 1000 이하로 제한하자.

    CHAR value;
    INT  count;
    if (!DecodeNumbers(body, length, &value, &count)) {
        // connection reset?
        return FALSE;
    }

    BYTE buf[1024] = { 0, 0, };

    if (count > 1000)
        count = 1000;

    // EncodeNumbers의 테스트
    int PROTOCOL_NUMBER = 123;
    count = EncodeNumbers(buf, PROTOCOL_NUMBER, 'A', 123);

    for (int i = 2; i < count; i++)
        buf[i] = value;

    socket->PutPacket(count, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.
    return TRUE;
}

static BOOL ProcLogin(BYTE *body, WORD length, CSocket *socket)
{
    char id[256];
    char passwd[256];

    if (!DecodeLogin(body, length, id, passwd)) {
        // connection reset?
        return FALSE;
    }

    //BYTE buf[1024] = { 0, 0, };

    // 디코딩에 성공한 다음에 인증 서버를 거쳐서 인증도 성공했다고 하고... (일단 OK)
    // 해당 소켓에 유저를 찾아서 할당해 준다

    CUser *user = new CUser;
    user->SetUserId(id[0]); // 첫글자의 아스키코드를 유저 아이디로 설정한다.
    AutoRelease(user);
    // TODO: 유저 정보를 DB에서 읽어서 해당 map에 넣어줘야!

    socket->SetUser(user);
    user->SetSocket(socket);

    user->EnterField(1); // 처음에 로그인하면 1번 방에 들어간다고 하자 일단. (로비?)

    return TRUE;
}

static BOOL ProcRoom(BYTE *body, WORD length, CSocket *socket)
{
    BYTE buf[1024] = { 0, 0, };

    DWORD ids[100]; // 쉽게 넘치는 것...
    // room 번호 리스트를 보내 준다.
    int count = gFieldManager->GetFieldIds(ids);    // BAD

    char *p = (char*)&buf[2];
    for (int i = 0; i < count; i++)
        p += sprintf_s(p, 1024, "%d ", ids[i]);
    p += sprintf_s(p, 1024, "\n");

    int len = p - (char *)buf;

    socket->PutPacket(len, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.

    return TRUE;
}

static BOOL ProcUser(BYTE *body, WORD length, CSocket *socket)
{
    BYTE buf[1024] = { 0, 0, };

    // 현재 유저가 있는 room에 같이 있는 유저들을 보내준다.
    CUser *user = socket->GetUser();    
    if (!user)
        return FALSE;
    AutoRelease(user);

    DWORD fieldId = user->GetFieldId();
    CField *field = gFieldManager->GetField(fieldId);
    if (!field)
        return FALSE;

    std::vector<DWORD> users;
    users.reserve(16);

    field->GetUserIds(users);

    char *p = (char*)&buf[2];
    for (int i = 0; i < users.size(); i++)
        p += sprintf_s(p, 1024, "%d ", users[i]);
    p += sprintf_s(p, 1024, "\n");

    int len = p - (char *)buf;

    socket->PutPacket(len, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.

    return TRUE;
}

static BOOL ProcEnter(BYTE *body, WORD length, CSocket *socket)
{
    DWORD fieldId;

    if (!DecodeEnter(body, length, &fieldId)) {
        return FALSE;
    }

    CUser *user = socket->GetUser();
    if (!user)
        return FALSE;
    AutoRelease(user);
    user->EnterField(fieldId);    // LeaveField는 EnterField 안에서.

    BYTE buf[1024] = { 0, 0, };

    // 현재 유저가 있는 room에 같이 있는 유저들을 보내준다.
    int len;
    memcpy(buf, "  OK\n", 5);
    len = 5;

    socket->PutPacket(len, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.

    return TRUE;
}

static BOOL ProcChat(BYTE *body, WORD length, CSocket *socket)
{
    char str[256];

    if (!DecodeChat(body, length, str)) {
        return FALSE;
    }

    BYTE buf[1024] = { 0, 0, };

    int len = strlen(str);
    memcpy(buf + 2, str, len);
    len += 2;

    CUser *user = socket->GetUser();
    if (!user)
        return FALSE;
    AutoRelease(user);

    DWORD fieldId = user->GetFieldId();
    CField *field = gFieldManager->GetField(fieldId);
    if (!field)
        return FALSE;

    std::vector<CUser *> users;
    users.reserve(16);

    field->GetUsers(users);

    // 방 안의 모든 사람에게 보낸다.
    for (int i = 0; i < users.size(); i++) {
        if (users[i]) {
            users[i]->GetSocket()->PutPacket(len, buf);
        }
    }
    //socket->PutPacket(len, buf);   // 여기서의 길이는 실제 전송할 패킷 길이이다.

    return TRUE;
}

typedef BOOL(*PacketFunction)(BYTE *, WORD, CSocket *);

typedef struct PacketTable {
    enum PROTOCOL   protocol;
    PacketFunction  function;
} PacketTable ;

// order by enum PROTOCOL
PacketTable packetTable[] = {
        { SUM,      ProcSum },
        { NUMBERS,  ProcNumbers },
        { LOGIN,    ProcLogin },
        { ROOM,     ProcRoom },
        { USER,     ProcUser },
        { ENTER,    ProcEnter },
        { CHAT,     ProcChat }
};

BOOL CPacket::ProcessPacket(BYTE *packet, WORD length, CSocket *socket)
{
    WORD protocol;
    protocol = *(WORD *)(packet + 2);     // size 두 바이트 다음의 두 바이트를 프로토콜로 사용
    // size(2) + protocol(2) 그에 대한 보정
    BYTE *body = packet   + 4;
    WORD bodyLen = length - 4;

    if (protocol >= END_PROTOCOL)
        return FALSE;

    packetTable[protocol].function(body, bodyLen, socket);
    return TRUE;
}

/// check packet table

static BOOL CheckPacketFunctions(VOID) {
    for (int i = 0; i < END_PROTOCOL; i++) {
        int protocol = packetTable[i].protocol;
        if (i != protocol) {
            assert(!"CheckPacketFunctions");
            return FALSE;
        }
    }
    return TRUE;
}

static CPacket check;

CPacket::CPacket()
{
    CheckPacketFunctions();
}