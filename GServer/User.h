#pragma once

class CSocket;

class CUser : public CGameObject {
public:
    CUser();

    BOOL SetSocket(CSocket *socket);
    CSocket *GetSocket();
    BOOL ClearSocket();

    BOOL SetUserId(DWORD64 userId);
    
    BOOL SetCharId(DWORD64 charId);

    BOOL Kick();

    BOOL EnterField(DWORD fieldId);
    BOOL LeaveField();

    DWORD GetFieldId() { return mFieldId; }


private:
    DWORD64     mUserId;    // 플랫폼에서 주는 유저 계정 id 값. 플랫폼 연동 위해 사용. 32bit or 64bit
    DWORD64     mCharId;    // World Id (2 BYTE) + Sequence number (6 BYTE). sequence number is not reused. 이 값은 캐릭터가 생성될 때  결정되는 외부값이다.
    CSocket     *mSocket;
    DWORD       mFieldId;
};

// charId를 4byte로 한다면, world id에 1 byte밖에 할당하지 못한다. 이 경우에 한 world에 생성할 수 있는 char 수는 1600만개 밖에 되지 못한다.
// 한 월드에서 offline인 유저를 포함해서 이 숫자는 충분한 숫자라고 보기 어렵다.
// 8byte로 한다면, world id에 2 byte를 할당하여, 하나의 게임에 대해 전세계에 생성되는 월드 수를 모두 감당할 수 있고, (1바이트 지역, 1바이트 서버)
// 한 월드 안에서 생성할 수 있는 캐릭터의 수도 충분히 큰 수가 된다.

// 게임 플레이에서 주로 사용되는 값은 GameObject의 mId 값일 것으로 생각한다.