﻿#include "stdafx.h"
#include "Server.h"

CServer::CServer()
{
    mListener = NULL;
    mNumConnection = 0;
    mNumAcceptSocket = 0;
}

BOOL CServer::Startup(VOID)
{
    if (!CIocp::Startup())
        return FALSE;

    mListener = new CSocketSrv();

    if (!mListener->Startup()) {
        Cleanup();
        return FALSE;
    }

    if (!mListener->Bind()) {
        Cleanup();
        return FALSE;
    }

    if (!mListener->Listen(DEFAULT_PORT, SOMAXCONN)) {
        Cleanup();
        return FALSE;
    }

    if (!RegisterSocket(mListener->GetSocket(), (ULONG_PTR)mListener)) {
        Cleanup();
        return FALSE;
    }
    
    for (int i = 0; i < MAX_SOCKET; i++) {
        mSockets[i] = NULL;
    }

    NewAcceptors();

    //for (int i = 0; i < MAX_SOCKET; i++) {
    //    mSockets[i] = new CSocket();
    //    if (!mSockets[i]->Initialize(mListener->GetSocket())) {    // Initialize 이걸 해야지 동작!!
    //        mSockets[i] = 0;
    //    }
    //    else 
    //        InterlockedIncrement(&mNumAcceptSocket);
    //}

    return TRUE;
}

BOOL CServer::NewAcceptors(VOID)    // 늘어나기만 하고, 줄어드는 로직은 없음. 개발 및 테스트 용도.
{
    static int last = 0;

    for (int i = last; i < MAX_SOCKET; i++) {

        if (mNumAcceptSocket >= 1000) {   // 기다리는 것이 1000개 되면 중단한다.
            last = i;
            break;
        }

        if (mSockets[i])
            continue;

        mSockets[i] = new CSocket();

        if (mSockets[i]->Initialize(mListener->GetSocket())) {    // Initialize 이걸 해야지 동작!!
            InterlockedIncrement(&mNumAcceptSocket);
        }
        else {
            mSockets[i] = NULL;
        }
    }
    return TRUE;
}

VOID CServer::Cleanup(VOID)
{
    if (!mListener)
        return;

    for (int i = 0; i < MAX_SOCKET; i++) {
        if (mSockets[i]) {
            mSockets[i]->Cleanup();
        }
    }
    mListener->Cleanup();

    SwitchToThread();
    //    Sleep(0); // Sleep(0)과 Sleep(1)의 차이. Sleep(0)에서는 switching이 발생하지 않을 수도 있다.

    for (int i = 0; i < MAX_SOCKET; i++) {
        delete mSockets[i];
        mSockets[i] = NULL;
    }
    delete mListener;
    CIocp::Cleanup();
}

VOID CServer::OnConnected(PVOID object)
{
//    OutputDebugString(L"Connect");
    
    CSocket *socket = (CSocket *)object;
    if (!RegisterSocket(socket->GetSocket(), (ULONG_PTR)socket))
        return;

    // 처음에 서버가 뭘 보내줘야 텔넷이 제대로 동작하는듯?
    // 마찬가지로 게임에서도 서버가 처음에 뭔가 보내 주도록? (버전 정보라던가)
    // ==> 서버가 처음에 아무 정보도 주지 않는 것으로 변경함
    //socket->PutPacket(1, (BYTE *)"]");

    if (!socket->OnConnected()) {
        return;
    }

    InterlockedIncrement(&mNumConnection);
    int n = InterlockedDecrement(&mNumAcceptSocket);

    if (n == 0) {   // accept 받을 수 있는 소켓이 하나는 남아 있어야 한다
        socket->Cleanup(); // 이것만 불러주면 나머지 disconnect 잘 처리된다!!!
    }
}

VOID CServer::OnDisconnected(PVOID object)
{
    if (gShutdownState)
        return;

//    OutputDebugString(L"Disconnect");
    InterlockedDecrement(&mNumConnection);

    CSocket *socket = (CSocket *)object;
    if (!socket->OnDisconnected(mListener->GetSocket()))
        return; // 다시 initialize 실패

    InterlockedIncrement(&mNumAcceptSocket);
    // 성공
}

VOID CServer::OnRead(PVOID object, DWORD dataLength)
{
//    OutputDebugString(L"Read");

    CSocket *socket = (CSocket *)object;

    socket->OnRead(dataLength);
}

VOID CServer::OnWrite(PVOID object, DWORD length)
{
//    OutputDebugString(L"Write");

    CSocket *socket = (CSocket *)object;
    socket->WriteCompleted(length);
}
