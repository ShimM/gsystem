#pragma once

/////////////////////////////////////////////////////////////////////////////////////////////////

const DWORD IOCP_PORT_MAX       = 16;      // IOCP PORT의 최대 수 (자료구조 크기)
const DWORD THREAD_PER_PORT      = 8;      // IOCP PORT 한 개에 할당되는 thread의 수

/////////////////////////////////////////////////////////////////////////////////////////////////

extern int GetWorkerThreadCount();  // worker thread에 대해서 thread id 구함
extern __declspec(thread) int gThreadId;

extern DWORD gThreadGetTickCount[]; // each thread's GetTickCount. for checking deadlock

/////////////////////////////////////////////////////////////////////////////////////////////////

// Queue 단위가 아니라 개별 object 단위로 serialize한다.
//#define OBJECT_SERIALIZE 

// message 값은 32비트 중에서 하위 16비트만 사용한다. 0~65535 
// 0x1000 = 4K, 0x10000 = 64K, 0x40000 = 256K, 0x100000 = 1M

const DWORD TASKLET_STACK_SIZE = 0x1000; // 4096  tasklet 안에서 tasklet 부르면? stack구조로 변경...

// Serial Queue마다 TimerQueueTimer를 하나씩 쓰는 방법으로 했더니 성능이 나오지 않아서, 타이머는 하나만 사용하고,
// 각 Serial Queue의 wheel을 모두 불러주는 방법으로 수정함
const DWORD SERIAL_QUEUE_COUNT = 4096;       // 언제나 &가 %보다 빠르다. 나머지를 사용해선 안된다.
const DWORD SERIAL_QUEUE_MASK = SERIAL_QUEUE_COUNT - 1;

struct MessageItem {  // 아마도 24 Byte?
    POBJECT  object;
    PVOID    param;
    WORD     message;
};

/////////////////////////////////////////////////////////////////////////////////////////////////

BOOL AddTasklet(POBJECT object, WORD message, PVOID param);

/// ITaskQueue : General과 Serial을 위한 인터페이스
class ITaskQueue {
public:
    virtual BOOL AddTask(POBJECT object, WORD message, PVOID param) = 0;
    virtual BOOL AddTimer(DWORD delta, POBJECT object, WORD message, PVOID param) = 0;
};

// BaseTaskQueue 내부 전용
const DWORD TIMER_QUEUE_COUNT = 1024;
const DWORD TIMER_QUEUE_MASK = TIMER_QUEUE_COUNT - 1;

class CBaseTaskQueue : public ITaskQueue {
public:
    CBaseTaskQueue(DWORD n = 0);
    ~CBaseTaskQueue();
    BOOL Startup();
    BOOL Cleanup();

    BOOL AddTask(POBJECT object, WORD message, PVOID param);
    BOOL AddTimer(DWORD delta, POBJECT object, WORD message, PVOID param);
    BOOL WorkerThread(int threadId);

public:
    BOOL AddTask_(POBJECT object, DWORD lmessage, PVOID param);

private:
    int		mThreadCount;

    HANDLE	mIocpHandles[IOCP_PORT_MAX];
    int		mIocpPortCount;
    int		mIocpPortMask;

    CTimerQueue mShortTimerQueue[TIMER_QUEUE_COUNT];
    CTimerQueue mLongTimerQueue;
private:
    HANDLE	mStartupEvent;
};

class CSerialTaskQueue : public ITaskQueue {
public:
    CSerialTaskQueue(CBaseTaskQueue *baseQueue = 0);
    ~CSerialTaskQueue();

    DWORD Initialize(CBaseTaskQueue *baseQueue);

    BOOL Startup();
    BOOL Cleanup();

    BOOL AddTask(POBJECT object, WORD message, PVOID param);
    BOOL AddTimer(DWORD time, POBJECT object, WORD message, PVOID param);
    BOOL Dispatch(POBJECT runObject);

private:
    IocpQueue       mIocpQueue;   // iocp handle을 대신하여.   
    CBaseTaskQueue *mBaseQueue;     // 시리얼 큐를 빠져나오면 넣는 parallel queue.
    CTimerQueue     mTimerQueue;    // AddTimer로 timer를 넣는다
    CRWLock         mSerialLock;  // queue 단위 => object 단위로 변경 (OBJECT_SERIALIZE)
    DWORD           mQueueIndex;    // parallel queue에 들어간 message도 이 값을 가지고 간다.
};

extern CBaseTaskQueue *gGlobalTaskQueue;    // multiples - 자신이 쓰레드를 생성하여 갖고 있으며, Task를 병렬로 실행한다

extern CBaseTaskQueue    *gMainTaskQueue;   // Serial Main - 별도로 한 개의 쓰레드를 가지고 순차적으로 실행한다. 

// 시리얼 큐를 미리 생성해 둔다. 이 큐 안에서 시리얼하게 실행되며, GlobalTaskQueue 위에서 동작한다.
// 큐 단위 시리얼라이즈나 객체 단위 시리얼라이즈를 위해 사용한다. (객체단위 시리얼라이즈는 객체에서 처리하는게 맞을듯)
extern CSerialTaskQueue  *gSerialTaskQueueArray[SERIAL_QUEUE_COUNT]; 

/////////////////////////////////////////////////////////////////////////////////////////////////

// Serial Task Queue를 위해 IOCP가 아니라 Circular Queue 사용? (속도는 빠르나 넘칠 수 있다)
//#define USE_CIRCULAR_QUEUE // 메모리 문제 때문에도 사용이 어렵겠다. IOCP는 놀랍다.
const DWORD CIRCULAR_QUEUE_SIZE = 0x40000;   // 4096은 쉽게 넘치고, 64K도 약간 불안 => 256K
const DWORD CIRCULAR_QUEUE_MASK = CIRCULAR_QUEUE_SIZE - 1;

// Serial에서 IOCP를 사용하고, 이것은 안쓰는 것으로.
class CCircularQueue {
public:
    CCircularQueue();
    VOID AddTask(POBJECT object, WORD message, PVOID param);
    BOOL GetTask(POBJECT *pObject, WORD *pParam, PVOID *pPtr);
    unsigned Length();

private:
    MessageItem queue[CIRCULAR_QUEUE_SIZE];
    DWORD     mHead, mTail;
    CRWLock   mLock;
};

// 이것들은 IOCP 안 쓰고 CTaskQueueGroup ==> CTaskQueue 구조로 생각해 본 것.
// Single Queue에 대한 blocking 문제를 피할 수 없다. 
// 그리고, Single Queue를 논리적으로 많이 쓰고 싶으면 그만큼의 쓰레드가 생성된다. 쓰지 않는다.
// CIRCULAR QUEUE의 메모리 부담도 너무 크다.

// circular queue를 사용하고, 1개 thread
class CTaskQueue {
public:
    CTaskQueue();
    ~CTaskQueue();
    void Startup();

    BOOL AddTask(POBJECT object, WORD message, PVOID param);
    BOOL Thread();
    unsigned Length();

private:
    MessageItem queue[CIRCULAR_QUEUE_SIZE];
    DWORD     mHead, mTail;
    HANDLE    mEvent;
};

// CTaskQueue를 병렬로 여러 개 늘어 놓은 것
class CTaskQueueGroup {
public:
    CTaskQueueGroup();
    ~CTaskQueueGroup();

    BOOL AddTask(POBJECT object, WORD message, PVOID param);    // 랜덤 큐에 넣음
    BOOL AddTaskSelect(POBJECT object, WORD message, PVOID param, unsigned select); // 특정 큐에 넣음 (해당 큐 안에서는 순차 실행). select 값을 hash하여 사용하므로, 큰 값을 줘도 된다.
    int  GetNumQueue();

private:
    int        mQueueCount;
    CTaskQueue *mQueue;
};
