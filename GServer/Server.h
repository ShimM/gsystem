#pragma once

class CServer : public CIocp
{
public:
    CServer();

    BOOL Startup(VOID);
    VOID Cleanup(VOID);
    BOOL NewAcceptors(VOID);

    virtual VOID OnConnected(PVOID object) override;
    virtual VOID OnDisconnected(PVOID object) override;

    virtual VOID OnRead(PVOID object, DWORD dataLength) override;
    virtual VOID OnWrite(PVOID object, DWORD dataLength) override;

    int NumConnection() { return mNumConnection; }
    int NumAcceptSocket() { return mNumAcceptSocket; }

private:
    CSocketSrv *mListener;
    CSocket    *mSockets[MAX_SOCKET];
    DWORD      mNumConnection;
    DWORD      mNumAcceptSocket;
};

extern CServer gNetServer;

