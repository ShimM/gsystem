#include "stdafx.h"
#if 0
BOOL CActor::AddTask(WORD message, PVOID param)
{
    DWORD hash = HashCode();
    CSerialTaskQueue *queue = gSerialTaskQueueArray[hash & SERIAL_QUEUE_MASK];

    if (!queue)
        return FALSE;

    queue->AddTask(this, message, param);

    return TRUE;
}

BOOL CActor::AddTimer(DWORD delta, WORD message, PVOID param)
{
    DWORD hash = HashCode();
    CSerialTaskQueue *queue = gSerialTaskQueueArray[hash & SERIAL_QUEUE_MASK];

    if (!queue)
        return FALSE;

    queue->AddTimer(delta, this, message, param);

    return TRUE;
}
#endif
