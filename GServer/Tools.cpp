#include "stdafx.h"
#include "Tools.h"

int GetNumberOfProcessors()
{
    static int sNumberOfProcessors = 0;

    if (sNumberOfProcessors)
        return sNumberOfProcessors;

    SYSTEM_INFO systemInfo;
    GetSystemInfo(&systemInfo);
    sNumberOfProcessors = systemInfo.dwNumberOfProcessors;

    return sNumberOfProcessors;
}

CRandom::CRandom(VOID)
{
}

CRandom::~CRandom(VOID)
{
}

BOOL CRandom::Init(UINT seed)
{
    srand(seed);
    return TRUE;
}

INT CRandom::Rand(VOID)
{
    return rand();
}

static CRandom sRandom;