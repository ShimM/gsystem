#pragma once

class CLock
{
public:
    CLock(VOID)                 { InitializeCriticalSectionAndSpinCount(&mCritical, 1000); }
    ~CLock(VOID)                { DeleteCriticalSection(&mCritical); }

    inline VOID Lock(VOID)      { EnterCriticalSection(&mCritical);  }
    inline VOID Unlock(VOID)    { LeaveCriticalSection(&mCritical);  }
    inline BOOL TryLock(VOID)   { return TryEnterCriticalSection(&mCritical); }

private:
    CRITICAL_SECTION mCritical;
};

#define TOKENPASTE_(x, y) x ## y
#define TOKENPASTE(x, y) TOKENPASTE_(x, y)

// 아무래도 AutoLock v(lock) 형식보다 AutoLock(lock) 형식이 낫겠다
#define AutoLock(lock) CAutoLock TOKENPASTE(_lock, __LINE__) (lock)

class CAutoLock {
public:
    inline CAutoLock(CLock &lock)
    {
        mLock = &lock;
        mLock->Lock();
    }

    inline ~CAutoLock()
    {
        mLock->Unlock();
    }
private:
    CLock *mLock;
};

// AutoLock과 같은 방식으로 AutoReadLock, AutoWriteLock도 정의할 수 있다

// recursive 기능은 없는 RWLock이다 (recursive 기능이 없는 것이 필요할 때도 있다)
class CRWLock {
public:
    CRWLock(VOID)                       { InitializeSRWLock(&mLock); }
    ~CRWLock(VOID)                      {}

    inline VOID LockShared(VOID)        { AcquireSRWLockShared(&mLock); }
    inline VOID UnlockShared(VOID)      { ReleaseSRWLockShared(&mLock); }
    inline BOOL TryLockShared(VOID)     { return TryAcquireSRWLockShared(&mLock); }

    inline VOID LockExclusive(VOID)     { AcquireSRWLockExclusive(&mLock); }
    inline VOID UnlockExclusive(VOID)   { ReleaseSRWLockExclusive(&mLock); }
    inline BOOL TryLockExclusive(VOID)  { return TryAcquireSRWLockExclusive(&mLock); }

private:
    SRWLOCK mLock;
};
