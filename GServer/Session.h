#pragma once

// 양방향으로 자주 호출될 것이므로,
// Socket - Session - User의 관계를 잘 정의해야 한다
// ==> 그냥 Socket과 User가 직접 연결하는 것으로

//class CUser;
//
//class CSessionItem {
//public:
//    CSessionItem() {
//        mSocket = NULL;
//        mUser = NULL;
//    }
//public:
//    CSocket     *mSocket;
//    CUser       *mUser;
//};
//
//extern int GetSession(CSocket *sock);