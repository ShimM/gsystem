#include "stdafx.h"

CQueue::CQueue()
{
    QueueNode *node = new QueueNode;        // dummy head node

    node->next = NULL;
    node->object = NULL;
    node->lmessage = 0;
    node->param = NULL;

    mHead = mTail = node;
}

CQueue::~CQueue()
{
    if (mHead)
        delete mHead;
}

// CAS 명령, p 포인터의 값이 old와 같으면 new로 변경하고 true를 리턴한다.
#define CAS(p, old, newv)    (InterlockedCompareExchangePointer(p, newv, old) == old)

// http://people.cs.pitt.edu/~jacklange/teaching/cs2510-f12/papers/implementing_lock_free.pdf
// 4페이지의 두번째 알고리즘을 구현하는 것이 더 낫겠다.

#if 0 // 첫번째 알고리즘
BOOL CQueue::Push(POBJECT object, WORD message, PVOID param)
{
    QueueNode *q = new QueueNode;
    q->next = NULL;
    q->object = object;
    q->message = message;
    q->param = param;

    // mTail->next = node
    BOOL succ;
    do {
        QueueNode *p = mTail;
        if (InterlockedCompareExchangePointer((PVOID*)&(p->next), q, NULL) != NULL) { // fail
            InterlockedCompareExchangePointer((PVOID*)&(mTail), p->next, p);
            continue;
        }
        break;
    }
    InterlockedCompareExchangePointer((PVOID*)&(mTail), p, q);

    return TRUE;
}
#endif

BOOL CQueue::Post(POBJECT object, DWORD lmessage, PVOID param)
{
    QueueNode *q = new QueueNode;

    q->next = NULL;
    q->object = object;
    q->lmessage = lmessage;
    q->param = param;

    QueueNode *p = mTail;
    QueueNode *oldp = p;

    // 하려는 일. 
    /*  p->next = q;
    mTail = q;
    return TRUE; */

    do {
        while (p->next)
            p = p->next;
    } while (!CAS((PVOID *)&(p->next), NULL, q));  // not null then continue

    CAS((PVOID *)&mTail, oldp, q); // mTail == oldp 이므로, 항상 q로 설정

    return TRUE;
}

BOOL CQueue::Get(POBJECT *pObject, DWORD *lmessage, PVOID *pParam, DWORD wait)
{
    QueueNode *p;
    p = mHead;
    if (p->next == NULL)
        return FALSE;
    /*
    mHead = p->next;
    QueueNode *node = p->next; // head는 dummy
    */
    do {
        p = mHead;
        if (p->next == NULL)
            return FALSE;
    } while (!CAS((PVOID *)&mHead, p, p->next));
    QueueNode *node = p->next;

    *pObject = node->object;
    *lmessage = node->lmessage;
    *pParam = node->param;

    delete p;

    return TRUE;
}
