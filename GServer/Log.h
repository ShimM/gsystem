#pragma once

#include <stdio.h>

#define MAX_BUFFER_LENGTH   4096

class CLog
{
public:
	static BOOL WriteLog(TCHAR *data, ...) {
		SYSTEMTIME systemTime;
		TCHAR currentDate[32]               = { 0, };
		TCHAR fileName[MAX_PATH]            = { 0, };
		FILE *filePtr                       = NULL;
		TCHAR debugLog[MAX_BUFFER_LENGTH]   = { 0, };

		va_list ap;
		TCHAR log[MAX_BUFFER_LENGTH]        = { 0, };

		va_start(ap, data);
        _vstprintf(log, MAX_BUFFER_LENGTH, data, ap);
		va_end(ap);

		GetLocalTime(&systemTime);
		_sntprintf_s(currentDate, 32, _T("%d-%d-%d %d:%d:%d"),
			systemTime.wYear,
			systemTime.wMonth,
			systemTime.wDay,
			systemTime.wHour,
			systemTime.wMinute,
			systemTime.wSecond);

		_sntprintf_s(fileName, MAX_PATH, _T("LOG_%d-%d-%d_%d.log"),
			systemTime.wYear,
			systemTime.wMonth,
			systemTime.wDay,
			systemTime.wHour);

        _tfopen_s(&filePtr, fileName, _T("a")); // file
		if (!filePtr)
			return FALSE;

		_ftprintf(filePtr, _T("[%s] %s\n"), currentDate, log);
		_sntprintf_s(debugLog, MAX_BUFFER_LENGTH, _T("[%s] %s\n"), currentDate, log);

		fflush(filePtr);
		fclose(filePtr);

		OutputDebugString(debugLog);            // debug 
		_tprintf(_T("%s"), debugLog);           // console

		return TRUE;
	}
};

/*
time_t rawtime;
struct tm timeinfo;
char buf[80];

time(&rawtime);
localtime_s(&timeinfo, &rawtime);
strftime(buf, 80, "%Y-%m-%d-%H-%M-%S", &timeinfo);
*/