#pragma once

// Timer Queue는 Task Queue의 컴포넌트로 사용된다. 해당 task queue로 add.
// Task Queue 외부의 독립적인 timer queue도 가능하다. 
// Map 별로 timer queue를 생성하고, 여기에 넣어준다.
// 그리고, Map이 삭제될 때 timer queue를 같이 삭제한다.

// delta in milisec. 42억 / 1000 = 420만초 = 1166시간 = 49일 = 7주
// http://randomascii.wordpress.com/2013/07/08/windows-timer-resolution-megawatts-wasted/

// overflow 방지 위해 처음 값을 저장해 두고 뺀다.
extern const DWORD gGetTickCountBase;

inline DWORD GetTickCountNow()
{
    return GetTickCount() - gGetTickCountBase;
}

class CObject;
typedef CObject *POBJECT;

////////////////////////////////////////////////////////////////////////////////

class CTimerObject {
public:
    CTimerObject(POBJECT object, DWORD time, WORD message, PVOID param);

    // Timer queue에 time, this, message, param 4개의 값을 저장한다
public:
    POBJECT  mObject;
    PVOID    mParam;
    DWORD    mTime;  // GetTickCount() - mBaseTime을 넣는다
    WORD     mMessage;
};

// Less를 overloading 하므로, 오른쪽의 것이 우선순위가 높으면 true여야 한다
struct CompareObject {
    inline bool operator()(CTimerObject const & obj1, CTimerObject const & obj2) {
        return obj1.mTime > obj2.mTime;
    }
};

// 내부에서 lock 하지 않고, Priority Queue 사용하는 쪽에서 lock 호출(pop에서 한꺼번에)
class CPriorityQueue
{
public:
    inline VOID Push(CTimerObject obj)  { mQueue.push(obj); }

    inline BOOL Empty(VOID)             { return mQueue.empty(); }

    inline CTimerObject Top(VOID)       { return mQueue.top(); }

    inline VOID Pop(VOID)               { mQueue.pop(); }

    BOOL Cleanup(VOID);

private:
    std::priority_queue<CTimerObject, std::vector<CTimerObject>, CompareObject> mQueue; // 일단 STL 사용해서 간단히
};

////////////////////////////////////////////////////////////////////////////////

class ITaskQueue;

class CTimerQueue {
public:
    CTimerQueue();
    BOOL Startup(ITaskQueue *);  // timer expire 되면, task queue의 AddTask를 불러준다
    BOOL Cleanup(VOID);
    BOOL AddTimer(POBJECT object, DWORD time, WORD message, PVOID param);
    VOID CALLBACK TimerRoutine(BOOLEAN TimerOrWaitFired);

private:
    CPriorityQueue mPriorityQueue;      // 여기서는 하나의 PriorityQueue에 하나의 TaskQueue가 대응하는 구조이다.
    CLock          mPriorityQueueLock;  // 한 TimerQueue로 들어오면 어느 TaskQueue로 갈지 결정된다. 다른 방법으로는 Object 가지고 Queue를 결정할 수 있다.

    ITaskQueue    *mTaskQueue;

private:
    static HANDLE sTimerQueue;  // Queue는 공통으로 하나 쓴다. 
    static HANDLE sTimer;       // 타이머도 한 개. Priority Queue는 CTimerQueue마다 하나씩
};

// wheel을 만든다. 65536개 슬롯. 1초에 32번. 30분 정도에 한 바퀴이다.
// GetTickCount 값은 msec 값이므로 1초에 32번이면 32로 나누면 된다. 그 다음에 wheel 크기로 나눈다.

//#define WHEEL_HZ    32
//#define WHEEL_SIZE  65536
//#define WHEEL_MASK  (WHEEL_SIZE - 1)