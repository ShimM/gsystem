#include "stdafx.h"
#if 0 

#define REF_CHECK 0x10000

static DWORD sequence = 0;

CZone::CZone()
{
}

CZone::CZone(WORD zoneType)
{
    if (gZoneManager)
    for (;;) {
        InterlockedIncrement(&sequence);
        mZoneId = (zoneType << 16) + WORD(sequence);
        if (gZoneManager->Exist(mZoneId))
            continue;
        break;
    }
}

CZone::~CZone()
{
}

BOOL CZone::PutObject(CGameObject *object)
{
    object->AddRef(REF_CHECK);

    if (object->GetType() == TYPE_PLAYER) {
        AutoLock(mUsersLock);
        mUsers.insert((CUser *)object);
    }
    else {
        AutoLock(mCreaturesLock);
        mCreatures.insert(object);
    }
    return TRUE;
}

BOOL CZone::RemoveObject(CGameObject *object)
{
    if (object->GetType() == TYPE_PLAYER) {
        AutoLock(mUsersLock);
        auto it = mUsers.find((CUser *)object);
        if (it != mUsers.end()) {
            object->Release(REF_CHECK);
            mUsers.erase((CUser *)object);
        }
    }
    else {
        AutoLock(mCreaturesLock);
        auto it = mCreatures.find((CUser *)object);
        if (it != mCreatures.end()) {
            object->Release(REF_CHECK);
            mCreatures.erase((CUser *)object);
        }
    }
    return TRUE;
}
    AutoLock(mUsersLock);

    for (auto it = mUsers.begin(); it != mUsers.end(); ++it)
        (*it)->AddTask(message, param);

    return TRUE;
BOOL CZone::Broadcast(WORD message, PVOID param)
{

}

int CZone::GetUserIds(DWORD *numbers)
{
    int count = 0;
    for (auto it = mUsers.begin(); it != mUsers.end(); ++it)
        numbers[count++] = (*it)->GetId();

    return count;
}

int CZone::GetUsers(CUser *users[])
{
    AutoLock(mUsersLock);

    int count = 0;
    for (auto it = mUsers.begin(); it != mUsers.end(); ++it) {
        users[count++] = (*it);
        //users[count]->AddRef(1);
    }

    return count;
}
#endif