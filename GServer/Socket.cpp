﻿#include "stdafx.h"

#define REF_CHECK   0x100

CSocket::CSocket()
{
    mUser = NULL;
}

CSocket::~CSocket()
{
    if (mUser) {
        mUser->Release(REF_CHECK);
        mUser = NULL;
    }
}

BOOL CSocket::Initialize(SOCKET listener)
{
    if (gShutdownState)    // 소켓 재시작시 검사
        return FALSE;

    if (!Startup())
        return FALSE;

    if (!Accept(listener))
        return FALSE;

    return TRUE;
}

BOOL CSocket::OnConnected(VOID)
{
    if (!DemandRead())
        return FALSE;

    return TRUE;
}

BOOL CSocket::OnDisconnected(SOCKET listener)
{
    if (mUser) {
        mUser->LeaveField();
        mUser->ClearSocket();
        mUser->Release(REF_CHECK);
        mUser = NULL;
    }

    Cleanup();
    return Initialize(listener);
}

// Read와 관련된 부분

BOOL CSocket::DemandRead(VOID)
{
    if (!mSocket)
        return FALSE;

    WSABUF  wsaBuf;           // 그래도 이건 있어야  
    DWORD   readBytes = 0;    // zero byte read
    DWORD   readFlag = 0;

    wsaBuf.buf = &mReadBuffer[mReadTail];
    wsaBuf.len = (int)(mReadEnd - mReadTail); // 버퍼 남은 공간 전부를 지정

    int ret = WSARecv(mSocket,
        &wsaBuf,
        1,
        &readBytes,
        &readFlag,
        &mReadOverlapped.overlapped,
        NULL);

    if (ret == SOCKET_ERROR) {
        int err = WSAGetLastError();
        if (err != WSA_IO_PENDING && err != WSAEWOULDBLOCK) {
            Cleanup();
            return FALSE;
        }
    }
    return TRUE;
}

BOOL CSocket::OnRead(DWORD length)
{
    mReadTail += length;        // Push: 읽었다는 콜백이므로, Tail에 값을 더한다

    WORD packetLength = 0;
    BYTE *packet = NULL;

    // 아래 루프의 방식이 Disruptor와 유사하다는 생각이 든다
    // 비동기적으로 큐에 여러 개를 쌓아놓고 한꺼번에 처리
    while (GetPacket(&packetLength, &packet)) {
        CPacket::ProcessPacket(packet, packetLength, this);
    }

    FixReadBuffer();   // 더이상 완성된 패킷이 없을 때

    if (!DemandRead())
        Cleanup();

    return TRUE;
}

VOID CSocket::FixReadBuffer()
{
    int len = (int)(mReadTail - mReadHead);

    if (len == 0) {
        mReadHead = mReadTail = 0;
    } 
    else {
        // 이 부분을 좀 더 가끔 옮길 수 있을지도 모른다 
        // 그렇지만 이 부분은 자주 불리지 않을 것이므로 이것으로 충분하다 (클라이언트는 NoDelay로 보낼 것) 
        MoveMemory(mReadBuffer, &mReadBuffer[mReadHead], len);
        mReadHead = 0;
        mReadTail = len;
    }
}

//////////

BOOL CSocket::GetPacket(OUT WORD *pPacketSize, OUT BYTE **pPacket)    // 앞에서 size 두자리 떼고 나머지 
{
    if (!pPacketSize || !pPacket)
        return FALSE;

    int bufLen = (int)(mReadTail - mReadHead);
    // 패킷 구조 = size(2) + body (n-2) : size는 전체 길이이다(n)
    // body 안에서 프로토콜을 잘라서 해석하는 것은 사용하는 쪽에서 알아서 한다 (ProcessPacket)

    if (bufLen < 2) // size와 protocol은 항상 있어야 하므로
        return FALSE;

    SHORT *buf = (SHORT *)&mReadBuffer[mReadHead];
    int sizeValue = *buf;   // 패킷 헤더에 명시된 길이

    if (sizeValue >= READ_BUFFER_LENGTH) {
        // TODO: disconnect this socket.
        Cleanup();

        return FALSE;
    }

    if (bufLen < sizeValue)   // 아직 충분히 오지 않았음
        return FALSE;

    // OK
    mReadHead += sizeValue;   // Pop: 충분히 왔으므로 패킷 크기만큼 잘라먹는다

    // return values
    *pPacketSize = sizeValue;
    *pPacket = (BYTE *)buf;

    return TRUE;
}


//////////
/// Write와 관련된 부분

#if 0 // PutPacket v1. linear - disconnect
BOOL CSocket::PutPacket(IN WORD length, IN BYTE *packet)
{
    AutoLock(mWriteLock);
    //mWriteLock.Enter();

    // 세션 버퍼에 복사하고, 버퍼를 send에 사용한다
    CHAR *buffer = &mWriteBuffer[mWriteTail];   // 복사 소스 시작 위치

    *(SHORT *)packet = length;  // 패킷 앞 2바이트에 길이 저장

    mWriteTail += length;

    // 클라이언트에서 못 받아가서 세션 버퍼가 넘치는 경우 접속을 끊음.
    if (mWriteTail >= mWriteEnd) {
        Cleanup();
        //mWriteLock.Leave();
        return FALSE;
    }

    CopyMemory(buffer, packet, length);
    Write(buffer, length);

    //mWriteLock.Leave(); // 앞으로 당길까 하는 생각을 했으나, Write 순서가 꼬일 수 있어서 안하기로!!
    return TRUE;
}
#endif

#if 1 // PutPacket v2. circular
BOOL CSocket::PutPacket(const IN WORD length, IN BYTE * const packet)
{
    AutoLock(mWriteLock); // PutPacket은 여러 쓰레드에서 불릴 수 있다.
    //mWriteLock.Enter();

    // 소켓의 버퍼에 복사하고, 버퍼를 send에 사용한다

    CHAR *buffer = &mWriteBuffer[mWriteTail & WRITE_BUFFER_MASK];   // 복사 소스 시작 위치

    *(SHORT *)packet = length;  // 패킷 앞 2바이트에 길이 저장

    DWORD oldTail = mWriteTail;
    mWriteTail += length;

    // 클라이언트에서 못 받아가서 세션 버퍼가 넘치는 경우 접속을 끊음
    // circular로 해도 버퍼가 넘치는 경우에만 접속을 끊음
    if (mWriteTail - mWriteHead >= WRITE_BUFFER_LENGTH)
        goto error;

    if ((oldTail & WRITE_BUFFER_MASK) < (mWriteTail & WRITE_BUFFER_MASK)) { // 끝을 넘어가지 않은 경우 
        CopyMemory(buffer, packet, length);
        Write(buffer, length);
    }
    else {  // 끝을 넘어간 경우
        int size2 = mWriteTail & WRITE_BUFFER_MASK;
        int size1 = length - size2;
        if (size1 < 0)
            goto error;

        CopyMemory(buffer, packet, size1);
        Write(buffer, size1);
        buffer = mWriteBuffer; // 다시 맨 앞으로
        CopyMemory(buffer, packet+size1, size2);
        Write(buffer, size2);

        // TODO : Write를 두 번 부르지 말고, WSASend 한번 사용하는 Write2? => 굳이 만들 필요는 없을 듯
    }

    //mWriteLock.Leave(); // 앞으로 당길까 하는 생각을 했으나, Write 순서가 꼬일 수 있어서 안하기로!!
    return TRUE;

error:
    Cleanup();
    //mWriteLock.Leave();
    return FALSE;
}
#endif 

// IOCP write도 BYTE stream을 처리해야 한다. 
BOOL CSocket::Write(CHAR *data, DWORD length)
{
    if (!mSocket)
        return FALSE;

    if (!data || length <= 0)
        return FALSE;

    WSABUF wsaBuf;
    DWORD writeBytes = 0;   // zero byte write
    DWORD writeFlag = 0;
    wsaBuf.buf = data;
    wsaBuf.len = length;

    int ret = WSASend(mSocket,
        &wsaBuf,
        1,
        &writeBytes,
        writeFlag,
        &mWriteOverlapped.overlapped,
        NULL);

        if (ret == SOCKET_ERROR) {
        int err = WSAGetLastError();
        if (err != WSA_IO_PENDING && err != WSAEWOULDBLOCK) {
            Cleanup();
            return FALSE;
        }
    }
    return TRUE;
}

VOID CSocket::WriteCompleted(DWORD length)
{
    AutoLock(mWriteLock);
    //mWriteLock.Enter();

    mWriteHead += length;
#if 1
    // 모든 write complete 되었을 때 head, tail을 0으로 설정
    int len = (int)(mWriteTail - mWriteHead);
    if (len == 0) {
        mWriteHead = mWriteTail = 0;
    }
#endif
    //mWriteLock.Leave();
}

//////////

BOOL CSocket::SetUser(CUser *user)
{
    if (!user)
        return FALSE;

    user->AddRef(REF_CHECK);
    mUser = user;
    
    return TRUE;
}

CUser *CSocket::GetUser()
{
    if (mUser)
        mUser->AddRef(1);

    return mUser;
}