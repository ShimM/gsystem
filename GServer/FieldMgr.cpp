#include "stdafx.h"

// Field Manager를 통해 Field를 생성하고 삭제한다

CFieldManager *gFieldManager = NULL;

CFieldManager::CFieldManager()
{
}

CFieldManager::~CFieldManager()
{
    Cleanup();
}

BOOL CFieldManager::Startup()
{
    // loading all template data from config.
    // loading persistent field

    return TRUE;
}

BOOL CFieldManager::Cleanup()
{
    AutoLock(mFieldsLock);
    for (auto it = mFields.begin(); it != mFields.end(); ++it)
        it->second->Release(1);
    mFields.clear();
    return TRUE;
}

BOOL CFieldManager::CreateField(WORD typeId, CField **pField)
{
    if (!pField)
        return FALSE;
    CField *field = *pField;

    // 생성하고
    field = new CField(typeId);
    
    DWORD fieldId = field->GetFieldId();

    // map에 추가
    AutoLock(mFieldsLock);
    mFields.insert(FieldMap::value_type(fieldId, field));

    return TRUE;
}

#if 0
BOOL CFieldManager::CloneField(CField *source, CField **pField)
{
    if (!pField)
        return FALSE;
    CField *field = *pField;

    // 이건 나중에

    return TRUE;
}
#endif


BOOL CFieldManager::Exist(DWORD fieldId)
{
    return mFields.find(fieldId) != mFields.end();
}

CField *CFieldManager::GetField(DWORD fieldId)
{
    auto it = mFields.find(fieldId);
    if (it == mFields.end())
        return NULL;

    return it->second;
}

int CFieldManager::GetFieldIds(DWORD *numbers)
{
    int count = 0;
    for (auto it = mFields.begin(); it != mFields.end(); ++it)
        numbers[count++] = it->first;

    return count;
}