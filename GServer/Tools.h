#pragma once

extern int GetNumberOfProcessors();

class CRandom
{
public:
	CRandom(VOID);
	~CRandom(VOID);

private:
	UINT mSeed;

public:
	BOOL Init(UINT seed);
	int	Rand(VOID);
};
