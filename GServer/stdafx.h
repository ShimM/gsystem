// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//
#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <assert.h>

// C++
#include <queue>
#include <hash_map>
#include <hash_set>

#include <WinSock2.h>
#include <MSWSock.h>

// TODO: reference additional headers your program requires here

#include "Define.h"
// Tools
#include "Lock.h"
#include "Log.h"
#include "MemLeak.h"
#include "Tools.h"
// Engine
#include "Iocp.h"
#include "SocketSrv.h"
#include "Socket.h"
#include "Server.h"
#include "Stream.h"
#include "Timer.h"
#include "Queue.h"
#include "TaskQueue.h"
#include "Object.h"     // after taskqueue
#include "Actor.h"
#include "CallStack.h" // after taskqueue
#include "Map.h"
//Game
#include "Packet.h"
#include "Session.h"
#include "GameObject.h"
#include "User.h"
#include "Npc.h"
#include "Zone.h"
#include "ZoneMgr.h"
#include "Field.h"
#include "FieldMgr.h"

extern BOOL gShutdownState;
