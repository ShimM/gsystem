#pragma once

// Ref Object Root Class - reference counting
class CRefObject {
public: 
    CRefObject();

    // AddRef, Release pair
    BOOL AddRef(int refCheck);
    BOOL Release(int refCheck);

    inline DWORD HashCode() const {         // https://gist.github.com/badboy/6267743
        UINT_PTR key = UINT_PTR(this);      // must be unsigned!
        //key *= 2654435761;
        key += (key << 4);                  // 홀수 곱셈 (*17)
        key ^= (key << 25) | (key >> 8);    // 위의 링크의 값 중에 LSB가 가장 많이 바뀔것 같은 것
        return DWORD(key);
    }

#ifndef _DEBUG  // Debug 모드에서는 new가 다르게 확장됨 (MemLeak.h)
    static void * operator new(size_t size);
    static void operator delete (void *p);
#endif

protected:
    virtual ~CRefObject(); // delete를 직접 호출하는 것을 금지한다

private:
#ifdef REF_DEBUG
    LONG64 mRefCheck;
#endif
    DWORD mRefCount;
};

typedef CRefObject *PREFOBJECT;

// new와 alloc으로 Heap에 할당한 것에 대해서만 사용해야 한다. 스택에 할당한 것에 대해 사용하면 crash.
//
// 1에서 0으로 될 때 다른 Pool로 옮겨놓고, 0에서 -1이 되는 것을 체크할 수도 있다
// 그러나, AddRef에서 타입별로 4bit 카운트를 사용하므로, 어지간해서는 발생하지 않을 것으로 생각됨
//
// http://blogs.msdn.com/b/timng/archive/2006/11/16/bad-com-practices-returning-an-addref-d-pointer.aspx
// GetInterface에서는 AddRef하여 돌려주면 안된다. 특히 스마트 포인터 용도로 AddRef하여 돌려주는 것은 
// Create로 시작하는 것으로 한정한다던가 스마트 포인터와 함께 사용하기 위해서 염두에 두어야 하는 부분이다
// (이름 규칙의 문제) - 이 글도 아래 0 기반 버전의 문제를 갖고 있다.
//
// 0 기반 버전:
// 처음에 alloc이나 new하면 0이고, AddRef를 거쳐 Release로 1 감소하며, 1에서 0으로 떨어지는 순간에 지워진다
// new에서 1을 주는 것보다 장점: AddRef~Release 짝이 맞으며, AutoRef 사용이 가능하다.
//
// 자료구조에서 빼내고 제거하는 경우에, Top과 Pop에 의한 방식으로 사용한다.
// Top에서 찾아서 받아간 쪽에서 reference를 증가시킨 다음에, Pop 하면서 자료구조의 reference를 감소시킨다
// 이렇게 함으로써 Top, Find 등의 구현이 간단해진다. Pop에서는 추가로 포인터를 리턴하지 않는다
//
// Top, Find에 AddRef 붙은 함수를 만들지 않는다.
// 어떤 함수 A에서 Find로 받아와서, AutoRef하면, A 함수 내부에서는 사용이 가능하다. A가 리턴하는 입장이면, AddRef하지 않는다.
// 타이머, 큐 등 자료구조에 추가할 때 AddRef하고, 자료구조에서 뺄 때 Release한다.
//
// 1 기반 버전:
// 위의 0 기반 버전은 serial 하면서 event driven일 때는 아무 문제 없이 동작할 수 있다.
// 그렇지만, Top, Find를 통해 포인터를 돌려준 다음에, 
// 다른 쓰레드에 의해서 자료구조에서 제거되어 객체를 참조하는 곳이 없어지면, (병렬 실행)
// Top과 Pop 사이에 이미 객체가 삭제된 상태가 될 수 있다. (Top을 받고 즉시 +1 한다고 해도)
//
// 따라서 new는 물론이고 포인터를 리턴받는 모든 함수 호출은 하나의 reference를 가지고 리턴되는 것을 디폴트로 해야 한다.
// 그리고, 그 결과는 즉시 AutoRelease를 걸어서, 리턴받은 컨텍스트가 끝나면 자동으로 해당 reference가 삭제되도록 한다.
// 이 안에서 리턴하려고 하면, AddRef를 해야 한다.


#define AutoRef(ptr) CAutoRef TOKENPASTE(_ref, __LINE__) (ptr)

class CAutoRef {
public:
    CAutoRef(VOID) : mPtr(0) {}

    CAutoRef(PREFOBJECT ptr)
    {
        if (ptr) {
            ptr->AddRef(1);
            mPtr = ptr;
        }
    }
      
    ~CAutoRef()
    {
        if (mPtr) {
            mPtr->Release(1);
            mPtr = 0;
        }
    }

private:
    PREFOBJECT mPtr;
};


#define AutoRelease(ptr) CAutoRelease TOKENPASTE(_ref, __LINE__) (ptr)

class CAutoRelease {
public:
    CAutoRelease(VOID) : mPtr(0) {}

    CAutoRelease(PREFOBJECT ptr)
    {
        if (ptr) {
            mPtr = ptr;
        }
    }

    ~CAutoRelease()
    {
        if (mPtr) {
            mPtr->Release(1);
            mPtr = 0;
        }
    }

private:
    PREFOBJECT mPtr;
};

const DWORD SERIAL_MSG_BEGIN = 0x8000;

// TODO: 현재 구성과 다르게, AddTask와 AddTimer의 구분을 message의 타입에 따라 할 수도 있겠다. 
// 이렇게 구분해서 동작하는 애를 suffix 없는 애로 빼고, message에 따라 ~Serial, ~Parallel을 나누어 호출할 수 있겠다.

// Object Class - Runnable, AddTask & Run
// 서버의 모든 실행 가능한 오브젝트

class CObject : public CRefObject {
    friend class CBaseTaskQueue;
    friend class CTaskletStack;
    friend class CTasklets;

public:     // AddTask / AddTimer, Run 

    // AddTask, AddTaskSerial과 같이, 자신에 대해서 즉시 호출하는 것은 AddTasklet을 이용하는 것이 바람직하다.

    BOOL AddTask(WORD message, PVOID param);    // parallel queue를 거쳐 여러 공용 쓰레드에서 실행한다
    BOOL AddTaskMain(WORD message, PVOID param);        // Serial Main Queue의 쓰레드에서 순차적으로 실행한다

    BOOL AddTimer(DWORD delta, WORD message, PVOID param);  // delta in milisec. 42억 / 1000 = 420만초 = 1166시간 = 48일
    BOOL AddTimerMain(DWORD delta, WORD message, PVOID param);

    BOOL AddTaskSerial(WORD message, PVOID param);        // object id의 해시값에 따라 serial queue를 선택한다
    BOOL AddTimerSerial(DWORD delta, WORD message, PVOID param);

    BOOL AddTask(ITaskQueue *queue, WORD message, PVOID param);    // queue를 지정하여 실행한다. queue의 타입에 따른다 
    BOOL AddTimer(ITaskQueue *queue, DWORD delta, WORD message, PVOID param);

public:
#ifdef OBJECT_SERIALIZE
    IocpQueue  mIocpQueue;   
    CRWLock    mSerialLock;    // serial lock per object. (CSerialTaskQueue 단위로부터 변경함)
#endif

protected:
    BOOL AddTasklet(WORD message, PVOID param);         // 현재 실행 중인 쓰레드의 끝부분에서 실행하며, 자신에 대해서만 실행할 수 있다. 
                                                        // Run 다음에, RunOut 전에 실행한다. 시리얼 경우에는 시리얼 실행한다.

protected:
    virtual WORD Run(WORD message, PVOID param) {      // Run의 리턴 값을 RunOut에 전달한다. 단, 0을 리턴하면 RunOut은 실행되지 않는다.
        return 0;  
    }     
    virtual WORD RunSerial(WORD message, PVOID param) {
        return 0;
    }
    virtual VOID  RunOut(WORD message) {}              // serial 밖에서 실행된다. 자신의 상태를 변경해서는 안된다. (const 강제는 잘 안됨)
};

typedef CObject *POBJECT;

