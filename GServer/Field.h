#pragma once

typedef stdext::hash_multiset<CGameObject *> CreatureSet;
typedef stdext::hash_multiset<CUser *>       UserSet;

class CField : public CRefObject {  // Field는 실행의 단위는 아니다. (그래서 RefObject)
public:
    CField();
    CField(WORD fieldType);
    ~CField();

    inline DWORD GetFieldId() { return mFieldId;  }

    BOOL PutObject(CGameObject *object);
    BOOL RemoveObject(CGameObject *object);

    BOOL MoveObject(CGameObject *object);
   
    BOOL Broadcast(WORD message, PVOID param);  // 모든 player에게 broadcasting 구현.
    BOOL Broadcast(WORD message, PVOID param, Point pt, WORD range);  // 반지름 range 내의 모든 player에게 broadcasting 구현.
    int  GetUserIds(std::vector<DWORD> &numbers);
    int  GetUsers(std::vector<CUser *> &users);

private:
    // 복사 생성자 금지
    CField(const CField&);
    CField& operator=(const CField&);

private:
    DWORD       mFieldId;   // 16 bit type id + 16 bit instance id

    UserSet     mUsers;     // User Players
    CLock       mUsersLock;

    CreatureSet mCreatures; // others
    CLock       mCreaturesLock;

private:
    CMap        mMap;  // Map은 Sector * Sector의 격자 안에 object들을 저장한다
};
