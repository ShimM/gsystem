#pragma once

class CUser;

class CSocket : public CSocketSrv
{
public:
    CSocket();
    ~CSocket();

    BOOL Initialize(SOCKET listener);
    BOOL OnConnected(VOID);
    BOOL OnDisconnected(SOCKET listener);

    BOOL DemandRead(VOID);
    BOOL OnRead(DWORD length);
    VOID FixReadBuffer(VOID);

    BOOL Write(CHAR *data, DWORD length);
    VOID WriteCompleted(DWORD length);

    BOOL GetPacket(OUT WORD *pPacketSize, OUT BYTE **pPacket);
    BOOL PutPacket(IN WORD length, IN BYTE *body);

    BOOL SetUser(CUser *user);
    CUser *GetUser(VOID);

private:
    CLock mWriteLock;

private:
    //DWORD   mSessionId; // Socket이 연결된 session. session 값은 재활용 간격이 매우 길다
    CUser   *mUser;     // session ID를 사용할 것인가 아니면 직접 유저 포인터를 가질 것인가
};