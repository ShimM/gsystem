#include "stdafx.h"

#define REF_CHECK 0x1000000

CMap::CMap()
{
    mXsize = 0;
    mYsize = 0;
    mSector = 0;
}

CMap::~CMap()
{
    delete[] mSector;
}

typedef CSector *SectorRow;

BOOL CMap::Initialize(WORD xsize, WORD ysize)
{
    mSector = new SectorRow[mXsize];
    for (int i = 0; i < mXsize; i++)
        mSector[i] = new CSector[mYsize];
    //mSector = new CSector[mXsize * mYsize];
    
    return TRUE;
}

CSector *CMap::GetSector(WORD xsec, WORD ysec)
{
    return &mSector[xsec][ysec];
}

BOOL CMap::PutObject(WORD xsec, WORD ysec, CGameObject *object)
{
    mSector[xsec][ysec].PutObject(object);
    
    return TRUE;
}

BOOL CMap::RemoveObject(WORD xsec, WORD ysec, CGameObject *object)
{
    mSector[xsec][ysec].RemoveObject(object);

    return TRUE;
}

BOOL CSector::PutObject(CGameObject *object)
{
    object->AddRef(REF_CHECK);

    AutoLock(mObjectsLock);
    mObjects.insert(object);

    return TRUE;
}

BOOL CSector::RemoveObject(CGameObject *object)
{
    AutoLock(mObjectsLock);
    auto it = mObjects.find((CUser *)object);
    if (it != mObjects.end()) {
        object->Release(REF_CHECK);
        mObjects.erase((CUser *)object);
    }
    return TRUE;
}
