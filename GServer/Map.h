#pragma once

// 최소한의 역할을 하는 Map을 일단 정의하자
// 그리고 이 공간을 Sector로 구분한다 - PC, NPC 포인터는 Sector 단위에 저장된다

// 2차원 map. 최대 크기 4km * 4km. 기본 단위는 1/16 m 로 하면, (4비트는 meter 이하이다)
// 4km는 4096 * 16 이므로, 16비트로 나타낼 수 있다
// 서버에서 좌표는 정수로 표현한다
// 실수는 사용하지 않는다

const WORD SECTOR_SIZE = 256; // 16 meter

struct Point {
    WORD x;
    WORD y;
};

class CSector;
class CGameObject;

class CMap {
public:
    CMap();
    ~CMap();
    BOOL Initialize(WORD xsize, WORD ysize);
    BOOL PutObject(WORD xsec, WORD ysec, CGameObject *object);
    BOOL RemoveObject(WORD xsec, WORD ysec, CGameObject *object);

    CSector *GetSector(WORD x, WORD y);

private:
    WORD mXsize, mYsize;
    CSector **mSector;
};

// 이 자료구조는 좋지 않다. double 혹은 single linked list로 바꾸는 것이 좋겠다.
typedef stdext::hash_multiset<CGameObject *> ObjectSet;

class CSector {
public:
    BOOL PutObject(CGameObject *object);
    BOOL RemoveObject(CGameObject *object);

private:
    ObjectSet   mObjects; // others
    CLock       mObjectsLock;
};


