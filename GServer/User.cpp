#include "stdafx.h"

CUser::CUser()
{
    mType = TYPE_PLAYER;

    mUserId = 0;
    mCharId = 0;
    mSocket = NULL;
    mFieldId = 0;
}

BOOL CUser::SetSocket(CSocket *socket)
{
    mSocket = socket;
    return TRUE;
}

CSocket *CUser::GetSocket()
{
    return mSocket;
}

BOOL CUser::ClearSocket()
{
    mSocket = NULL;
    return TRUE;
}

BOOL CUser::Kick()
{
    //mSocket->Cleanup();

    return TRUE;
}

BOOL CUser::SetUserId(DWORD64 userId)
{
    mUserId = userId;
    return TRUE;
}

BOOL CUser::SetCharId(DWORD64 charId)
{
    mCharId = charId;
    return TRUE;
}

BOOL CUser::LeaveField()
{
    if (!mFieldId)
        return FALSE;

    CField *field = gFieldManager->GetField(mFieldId);
    if (!field)
        return FALSE;

    field->RemoveObject(this);
    return TRUE;
}

BOOL CUser::EnterField(DWORD fieldId)
{
    LeaveField();

    mFieldId = 0;

    CField *field = gFieldManager->GetField(fieldId);
    if (!field)
        return FALSE;

    field->PutObject(this);

    mFieldId = fieldId;

    return TRUE;
}

