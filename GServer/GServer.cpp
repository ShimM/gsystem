// GServer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "GServer.h"
#include <mmsystem.h>

#pragma comment(lib,"winmm.lib")

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

// My Global Variables:
CServer gNetServer;
BOOL gShutdownState = FALSE;

// My functions at the end:
static BOOL         MyStartup();
static BOOL         MyCleanup();
VOID                MyScreen(HDC hdc);
VOID                MyTimer();
VOID                MyTest();


int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_GSERVER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

    MyStartup();

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_GSERVER));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

    MyCleanup();

	return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_GSERVER));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_GSERVER);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;

    switch (message)
    {
    case WM_COMMAND:
        wmId = LOWORD(wParam);
        wmEvent = HIWORD(wParam);
        // Parse the menu selections:
        switch (wmId)
        {
        case IDM_ABOUT:
            DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
            break;
        case IDM_EXIT:
            DestroyWindow(hWnd);
            break;
        default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        break;
    case WM_CREATE:
        SetTimer(hWnd, 1, 1000, NULL);
        break;
    case WM_TIMER:
        if (wParam == 1) {  // 1 sec
            MyTimer();
            InvalidateRect(hWnd, NULL, TRUE);
            UpdateWindow(hWnd);
        }
        break;
    case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
        MyScreen(hdc);
        EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}

//////////////////////////////////////////////////////////////////////////////////////

// setup timer resolution
BOOL SetupTimerResolution()
{
    const int TARGET_RESOLUTION = 1;         // 1-millisecond target resolution

    TIMECAPS tc;
    UINT     wTimerRes;

    if (timeGetDevCaps(&tc, sizeof(TIMECAPS)) != TIMERR_NOERROR)
    {
        // Error; application can't continue.
        exit(1);
    }

    wTimerRes = min(max(tc.wPeriodMin, TARGET_RESOLUTION), tc.wPeriodMax);
    timeBeginPeriod(wTimerRes);

    return TRUE;
}

BOOL MyStartup()
{
    // winsock
    WSADATA wsaData;
    WSAStartup(MAKEWORD(2, 2), &wsaData);

    // timer
    SetupTimerResolution();

    // task queue
    // Parallel Global Task Queue
    gGlobalTaskQueue = new CBaseTaskQueue();    // 자신이 쓰레드를 생성하여 갖고 있으며, Task를 병렬로 실행한다
    // Serial Main Task Queue
    gMainTaskQueue = new CBaseTaskQueue(1);     // 전용의 한 개의 쓰레드를 가지고 순차적으로 실행한다.

    for (int i = 0; i < SERIAL_QUEUE_COUNT; i++) {
        gSerialTaskQueueArray[i] = new CSerialTaskQueue(gGlobalTaskQueue);
    }

    // field manager
    gFieldManager = new CFieldManager();
    //gZoneManager = new CZoneManager();

    CField *field;
    // 테스트 용도의 field 2개 생성한다.
    gFieldManager->CreateField(0, &field);
    gFieldManager->CreateField(0, &field);

    // network server
    gNetServer.Startup();

    MyTest();

    //CLog log;
    //log.WriteLog(L"Test");  // 잘 되네

    return TRUE;
}

BOOL MyCleanup()
{
    gShutdownState = TRUE;

    gNetServer.Cleanup();
    WSACleanup();

    delete gFieldManager;
//    delete gZoneManager;

    delete gMainTaskQueue;
    delete gGlobalTaskQueue;

    for (int i = 0; i < SERIAL_QUEUE_COUNT; i++) {
        delete gSerialTaskQueueArray[i];
    }

    return TRUE;
}

#define Out(str)    (TextOut(hdc, 0, y, str, (int)wcslen(str)), (y+=16))

void MyScreen(HDC hdc)
{
    wchar_t str[256];
    int y = 0;

    wsprintf(str, L"Connected: %d Accept: %d", gNetServer.NumConnection(), gNetServer.NumAcceptSocket());
    Out(str);
    wsprintf(str, L"NPC: %d", gNpcCount);
    Out(str);
    wsprintf(str, L"NPC tasking: %d", gNpcRunCount);
    Out(str);
}

void MyTimer()
{
    // 데드락 체크는 worker에서 자신의 딜레이를 검사할 수 없다. 메인쓰레드에서 한다.
    DWORD now = GetTickCountNow();
    for (int i = 0; i < GetWorkerThreadCount(); i++) {
        if (gThreadGetTickCount[i] == 0)    // in GQCS
            continue;
        if (now - gThreadGetTickCount[i] >= 5000) { // deadlock
            DumpCallStack();
        }
    }

    gNetServer.NewAcceptors();
}


// My TEST
// Object의 생성과 삭제, NPC의 생성과 삭제

VOID MyTest()
{
    CField *field = new CField;
    AutoRelease(field);

    POBJECT obj = new CObject;
    AutoRelease(obj);
    obj->AddTimer(1000, 0, NULL);

    CNpc *npc[10000]; // for test
    for (int i = 0; i < 10000; i++) {
        npc[i] = new CNpc;
        AutoRelease(npc[i]);
        //npc[i]->AddTimer(1000 + rand() % 1000, 1, 0);
        field->PutObject(npc[i]);
        field->RemoveObject(npc[i]);
    }

    //POBJECT xxx = new CObject;

    //CField *field;
    //gFieldManager->CreateField(100, &field);


    //for (int i = 0; i < 10000; i++) {
    //    test[i] = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);
    //    for (int j = 0; j < 1000; j++)
    //        PostQueuedCompletionStatus(test[i], 1000, NULL, NULL);
    //}


}

