﻿#include "stdafx.h"
#include <process.h>

static unsigned __stdcall ThreadFunction(LPVOID lpParam);

CIocp::CIocp(DWORD n)
{
	mIocpHandle = NULL;
	mThreadCount = 0;
	mStartupEvent = NULL;

    if (n == 0)
        n = GetNumberOfProcessors();

    mThreadCount = n;
}

BOOL CIocp::Startup(VOID)
{
    // iocp에서 network 관련된 부분만 처리하므로 숫자 줄임. task queue thread들은 별도
    mIocpHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, mThreadCount);

	if (!mIocpHandle)
		return FALSE;

	mStartupEvent = CreateEvent(0, FALSE, FALSE, 0);
	if (mStartupEvent == NULL) {
		Cleanup();
		return FALSE;
	}

	for (int i = 0; i < mThreadCount; i++) {
		_beginthreadex(NULL, 0, ::ThreadFunction, this, 0, NULL);
 		WaitForSingleObject(mStartupEvent, INFINITE);   // 순차 실행
	}
	return TRUE;
}

BOOL CIocp::Cleanup(VOID)
{
    if (mIocpHandle)
        CloseHandle(mIocpHandle);
    if (mStartupEvent)
        CloseHandle(mStartupEvent);

	return FALSE;
}

BOOL CIocp::RegisterSocket(SOCKET socket, ULONG_PTR completionKey)
{
    if (!socket || !completionKey)
        return FALSE;

    mIocpHandle = CreateIoCompletionPort((HANDLE)socket, mIocpHandle, completionKey, 0);

    if (!mIocpHandle)
        return FALSE;

    return TRUE;
}

unsigned _stdcall ThreadFunction(LPVOID ptr)
{
    if (!ptr)
        return -1;

    ((CIocp *)ptr)->WorkerThread();
    return 0;
}

VOID CIocp::WorkerThread(VOID)
{
    SetEvent(mStartupEvent);

    BOOL success = FALSE;
    DWORD byteTransferred = 0;
    PVOID completionKey = NULL;
    OVERLAPPED *overlapped = NULL;
    OVERLAPPED_EX *overlappedEx = NULL;
    PVOID object = NULL;

    while (TRUE) {
        success = GetQueuedCompletionStatus(
            mIocpHandle,
            &byteTransferred,
            (PULONG_PTR)&completionKey,
            &overlapped,
            INFINITE);

        if (!completionKey) // key가 NULL인 경우 종료.
            continue;

        overlappedEx = (OVERLAPPED_EX *)overlapped;
        if (!overlappedEx)
            return;
        object = overlappedEx->object;

        if (!success || !byteTransferred) {
            if (overlappedEx->ioType == IO_ACCEPT)
                OnConnected(object);      // 클라이언트를 accept 경우
            else
                OnDisconnected(object);   // 클라이언트가 종료하는 경우
            continue;
        }

        switch (overlappedEx->ioType) {
        case IO_READ:
            OnRead(object, byteTransferred);
            break;
        case IO_WRITE:
            OnWrite(object, byteTransferred);
            break;
        }
    }
}

// 윈속 프로그래밍 기본
// http://www.joinc.co.kr/modules/moniwiki/wiki.php/Site/win_network_prog/doc/winsock_basic