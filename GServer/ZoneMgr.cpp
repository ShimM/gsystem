#include "stdafx.h"
#if 0
CZoneManager *gZoneManager = NULL;

CZoneManager::CZoneManager()
{
}

CZoneManager::~CZoneManager()
{
}

BOOL CZoneManager::CreateZone(WORD typeId, CZone **pZone)
{
    if (!pZone)
        return FALSE;
    CZone *zone = *pZone;

    // 생성하고
    zone = new CZone(typeId);

    DWORD zoneId = zone->GetZoneId();

    // map에 추가
    AutoLock(mZonesLock);
    mZones.insert(ZoneMap::value_type(zoneId, zone));

    return TRUE;
}

BOOL CZoneManager::Exist(DWORD zoneId)
{
    return mZones.find(zoneId) != mZones.end();
}

CZone *CZoneManager::GetZone(DWORD zoneId)
{
    auto it = mZones.find(zoneId);
    if (it == mZones.end())
        return NULL;

    return it->second;
}

int CZoneManager::GetZoneIds(DWORD *numbers)
{
    int count = 0;
    for (auto it = mZones.begin(); it != mZones.end(); ++it)
        numbers[count++] = it->first;

    return count;
}
#endif