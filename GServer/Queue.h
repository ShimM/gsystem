#pragma once 

class IocpQueue {
public:
    IocpQueue() { 
        mIocpHandle = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1);     // 0 if error
    }

    ~IocpQueue() {
        if (mIocpHandle)
            CloseHandle(mIocpHandle);
    }

    BOOL Post(POBJECT object, DWORD lmessage, PVOID param) {
        BOOL success = PostQueuedCompletionStatus(
                        mIocpHandle,
                        lmessage,
                        (ULONG_PTR)object,
                        (LPOVERLAPPED)param
                        );
        return success;
    }

    BOOL Get(POBJECT *object, DWORD *lmessage, PVOID *param, DWORD wait) {
        BOOL success = GetQueuedCompletionStatus(
                        mIocpHandle,
                        lmessage,    
                        (PULONG_PTR)object, 
                        (LPOVERLAPPED *)param, 
                        wait);  
        return success;
    }

private:
    HANDLE mIocpHandle;
};

/////////////////////////////////////////////////////////////////////////////////////////////////

struct QueueNode {
    struct QueueNode *next;
    POBJECT  object;
    PVOID    param;
    DWORD     lmessage;
};

// Head에서 pull하고, Tail에서 push한다. 
// Head에서 Tail 방향으로 향하는 단방향 리스트이다.

class CQueue {
public:
    CQueue();
    ~CQueue();
    //    void Startup();

    BOOL Post(POBJECT object, DWORD lmessage, PVOID param);
    BOOL Get(POBJECT *object, DWORD *lMessage, PVOID *param, DWORD wait);

private:
    QueueNode *mHead, *mTail;
};
