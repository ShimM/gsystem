#include "stdafx.h"

// NPC의 전체 리스트를 갖고 있지는 못하겠고, 갯수만 챙긴다
DWORD gNpcCount = 0;

DWORD gNpcRunCount = 0; // Npc의 Run 호출 횟수 

CNpc::CNpc()
{
    mType = TYPE_NPC;

    InterlockedIncrement(&gNpcCount);

    AddTimer(1000 + rand() % 1000, 1, 0);
}

CNpc::~CNpc()
{
    InterlockedDecrement(&gNpcCount);
}

extern VOID DumpCallStack();

VOID function()
{
    ENTRY;
    //if (gThreadId==0)    // for deadlock test.
    //    Sleep(10000);   
}

VOID function1()
{
    ENTRY;
    function();
}

// 사용법 가이드라인
// 여기서의 '상태'는 object 내의 모든 변수라기 보다, 인터페이스에 관련된 특정 변수로 보는 것이 좋겠다.
// 0) 기본 사용법
// RunSerial: AddSerial에서 불리며, (또는 SERIAL_MSG_BEGIN) 어떤 object의 RunSerial은 한 순간에 하나만 실행된다. 
// object의 상태 변경은 이 안에서 실행한다. 다만 상태 변경의 중간 상태는 외부에 보이지 않아야 한다. RunUpdate라고 부를 수도?
// 예를 들어, To Be 상태를 메모리에 만들고, interlocked 명령으로 한번에 바꿔친다.
// 1)
// 이렇게 중간 상태가 외부에 보이지 않는 경우에, AddTask나 정의된 다른 함수를 외부에서 호출할 수 있는데, 
// AddTask는 queue를 통해 병렬 실행, 다른 함수는 임의 context에서 서브루틴 호출되는 경우이다.
// 이들 함수에서는 object의 상태 변경을 수행하지 않으며, object를 상수로 다루어야 한다. lock free 자료구조에 액세스한다.
// 이 방법은, object의 상태 변경은 서로 충돌하며, 읽기는 가능한, RCU방식의 양방향 리스트 업데이트 등에 사용될 수 있다.
// 2)
// 그러나, RunSerial에서 업데이트를 할 때 중간에 자료구조가 깨지는 것이 외부에서 보이는 경우에,
// 이 자료구조에 접근을 AddTask, 다른 함수를 통해 임의 상황에서 병렬로 실행할 수 없다. 이런 부분은 다른 AddSerial에 의해서만 처리되어야 한다. - 완전 순차
// 3)
// 반대로, 어떤 자료구조에 대해 업데이트까지 lock free로 가능한 경우가 있다. 단순 정수와 같은 경우이다.
// 이 경우에는 해당 자료구조에 대해서 Read, Update 모두에 대해서 AddTask나 함수 호출 인터페이스를 제공하는 것이 가능하다. - 완전 병렬 실행 가능
// 
// 이렇게 세가지 경우 외에 또 다른 경우가 있을까?

// Serial과 Parallel 타입의 메시지는 같은 값을 갖지 않는 것이 좋다. AddTask, AddSerial을 잘못 쓰면 아주 오동작을 할 수 있다.
enum {
    MSG1 = 1,
    MSG2,
    MSG3,
    MSG4,
    SERIAL0 = SERIAL_MSG_BEGIN,   // 최상위 비트가 1이면 Serial이다.
    SERIAL1,
    SERIAL2,
};

WORD CNpc::RunSerial(WORD message, PVOID param)
{
    ENTRY;
    function1();

    InterlockedIncrement(&gNpcRunCount);

    switch (message) {
    case SERIAL0:
        //AddTimer(1000, MSG2, 0);
        break;
    case SERIAL1:
        break;
    }
    return 0;   // RunOut 실행하지 않음
}

WORD CNpc::Run(WORD message, PVOID param)
{
    ENTRY;
    function1();

    InterlockedIncrement(&gNpcRunCount);

    switch (message) {
    case MSG1:
        AddTimer(10, MSG2, 0);
        AddTask(SERIAL0, 0); // 아래와 동일하게 동작하도록 수정. 하지만 자신에게 AddTask 부르는건 불필요한 일이다.
        // AddTaskSerial(SERIAL0, 0);
        AddTimer(10, SERIAL1, 0);
        break;
    case MSG2:
        AddTimer(10, MSG3, 0);
        AddTasklet(MSG4, 0); // AddTasklet은 Run와 RunSerial 안에서 호출할 수 있다. 현재 쓰레드의 끝에서 (RunOut 전에, 시리얼은 시리얼 모드에서) 한꺼번에 실행된다. MainQ 경우 thread가 하나이므로 마찬가지다.
        break;
    case MSG3:
        AddTaskMain(MSG4, 0); // AddSerialTask 쓰레드 문제 해결함
        AddTimerMain(10, MSG4, 0);
        break;
    //case 4:
    //    AddTimer(500, MSG5, 0);
    }
    return 0;   // RunOut 실행하지 않음
}

VOID CNpc::RunOut(WORD message)
{
    InterlockedIncrement(&gNpcRunCount);
    switch (message) {
    case MSG2:
        AddTimer(10, MSG3, 0);
        AddTasklet(MSG4, 0);
        break;
    }
}
