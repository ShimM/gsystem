#include "stdafx.h"

// https://code.google.com/p/simulationcraft/ 이런 것도 있다

const DWORD gGetTickCountBase = GetTickCount();

#define REF_CHECK 0x10000

// timer period in msec.
#define TIMER_PERIOD    1 
// 1초에 1000번 => timer의 수가 늘어나면서, 타이머 발생 빈도를 낮출 필요가 생겨 10으로 변경.
// 잘못 접근했다! => timer queue와 timer는 전체적으로 하나만 두고, 이 타이머는 1초에 1천번 깨어난다.
// Sleep(1)인 thread도 생각할 수 있으나, 그 경우에는 한번 처리할 때 작업량이 많으면 그 다음 타이머가 늦어진다는 차이가 있다. (딱히 문제가 안될 수도 있다)

CTimerQueue::CTimerQueue()
{
    sTimerQueue = 0;
    mTaskQueue = NULL;
}

static VOID CALLBACK TimerCallback(PVOID lpParam, BOOLEAN TimerOrWaitFired);

HANDLE CTimerQueue::sTimerQueue = NULL;	// 공통 타이머 큐
HANDLE CTimerQueue::sTimer = NULL;		// 공통 타이머

static std::vector<CTimerQueue *>sTimerQueues;
const DWORD TIMER_QUEUE_RESERVE = SERIAL_QUEUE_COUNT * 2;

BOOL CTimerQueue::Startup(ITaskQueue *taskQueue)
{
    mTaskQueue = taskQueue;	// 타이머가 끝나면 넣어줄 queue

    // CTimerQueue마다 queue를 만들 필요 없다. 공통으로 queue 하나 사용, QueueTimer만 각각 있으면 됨.
    // 별도의 CreateTimerQueue를 한 개 사용하는 것은 중요하다. 디폴트로 하면, 윈도우 처리 때문에 서버가 느려질 수 있다.
    if (!sTimerQueue)
        sTimerQueue = CreateTimerQueue();   
    if (!sTimerQueue)
        return FALSE;

    // 1초에 1,000번 부른다. 마지막 옵션을 주지 않으면, 쓰레드가 매우 많이 생성된다. WT_EXECUTEINTIMERTHREAD / WT_EXECUTEINPERSISTENTTHREAD ?
    // http://www.microsoft.com/msj/0499/pooling/pooling.aspx

    // 이 Timer도 CTimerQueue마다 하나씩 있을 필요는 없고, Wheel만 관리하면 된다.
    if (!sTimer) {
        if (!CreateTimerQueueTimer(&sTimer, sTimerQueue, (WAITORTIMERCALLBACK)TimerCallback, 0, 0, TIMER_PERIOD, WT_EXECUTEINPERSISTENTTHREAD))
            return FALSE;

        sTimerQueues.reserve(TIMER_QUEUE_RESERVE);
    }

    // this를 모으자 (CTimerQueue는 동적으로 만들고 삭제하고 한다기 보다, 한번 구조물을 만든 다음에 서비스하는 개념으로 접근한다. 삭제는 프로그램 종료할때만)
    sTimerQueues.push_back(this);

    return TRUE;
}

BOOL CTimerQueue::Cleanup()
{
    AutoLock(mPriorityQueueLock);
    mPriorityQueue.Cleanup();

    //if (sTimerQueue)	// 한번 설정하고 계속 유지한다
    //    DeleteTimerQueueEx(sTimerQueue, NULL);
    //sTimerQueue = 0;
    
    return TRUE;
}

BOOL CTimerQueue::AddTimer(POBJECT object, DWORD delta, WORD message, PVOID param)
{
    if (gShutdownState)    // AddTimer에서
        return FALSE;

    if (delta < 0)
        return FALSE;

    DWORD time = GetTickCountNow() + delta;     // GetTickCount() overflow 방지

    object->AddRef(REF_CHECK);   // 타이머가 갖는 ref count. 이것은 task 실행 후 release (TaskQueue)
    CTimerObject obj(object, time, message, param);

    AutoLock(mPriorityQueueLock);
    mPriorityQueue.Push(obj);

    return TRUE;
}

VOID CALLBACK CTimerQueue::TimerRoutine(BOOLEAN TimerOrWaitFired)
{
    if (gShutdownState)    // TimerRoutine에서
        return;

    // 여기서 timer queue에서 expire된 것들을 처리한다
    // queue에서 now보다 작은 것들을 모두 꺼내어 처리한다

    AutoLock(mPriorityQueueLock);

    if (mPriorityQueue.Empty()) {
        return;
    }

    DWORD now = GetTickCountNow();

    for (;;) {
        // 맨 처음 한번의 empty 검사는 위에서 이미 했음

        CTimerObject obj = mPriorityQueue.Top();
        if (now < obj.mTime)
            break;

        mPriorityQueue.Pop(); // 여기서 바로 pop해야한다
        POBJECT object = obj.mObject;

        // (테스트용도) 바로 Run 호출한다 -> 호출하고 Release() 해야 한다
        //object->Run(obj.mParam, obj.mPtr);

        // Timer가 속한 Task queue에 AddTask로 넣는다. (GCD 참조)
        // 타이머에서 task queue로 이전했으므로, task queue에서 ref count 처리한다?
        // 아닌듯! 어떤 자료구조이든, Add하는 함수는 내부에서 AddRef 부른다.

        mTaskQueue->AddTask(object, obj.mMessage, obj.mParam);
        object->Release(REF_CHECK); // AddTimer에 대응

        if (mPriorityQueue.Empty())
            break;
    }
}

static VOID CALLBACK TimerCallback(PVOID lpParam, BOOLEAN TimerOrWaitFired)
{
    //CTimerQueue *timerq = (CTimerQueue *)lpParam;
    //timerq->TimerRoutine(TimerOrWaitFired);
    // 결국 원래 하던 일이 각 TimerQueue의 콜백을 불러주는 역할이므로.
    for (int i = 0; i < sTimerQueues.size(); i++) {
        sTimerQueues[i]->TimerRoutine(TRUE);
    }
}

CTimerObject::CTimerObject(POBJECT object, DWORD time, WORD message, PVOID param)
{
    mObject = object;
    mTime = time;
    mMessage = message;
    mParam = param;
}

BOOL CPriorityQueue::Cleanup()
{
    while (!Empty()) {
        CTimerObject obj = Top();
        obj.mObject->Release(REF_CHECK);
        Pop();
    }
    return TRUE;
}
