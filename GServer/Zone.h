#pragma once
#if 0
typedef stdext::hash_multiset<CGameObject *> CreatureSet;
typedef stdext::hash_multiset<CUser *>       UserSet;

class CZone : public CRefObject {   // Zone은 실행의 단위는 아니다.
public:
    CZone();
    CZone(WORD zoneType);
    ~CZone();

    inline DWORD GetZoneId() { return mZoneId; }

    BOOL PutObject(CGameObject *object);
    BOOL RemoveObject(CGameObject *object);

    BOOL Broadcast(WORD message, PVOID param);  // zone의 모든 player에게 broadcasting 구현.
    int GetUserIds(DWORD *numbers);
    int GetUsers(CUser *users[]);

private:
    // 복사 생성자 금지
    CZone(const CZone&);
    CZone& operator=(const CZone&);

private:
    DWORD       mZoneId;   // 16 bit type id + 16 bit instance id

    // PC - Player Character, NPC - Non Player Character
private:
    UserSet     mUsers;     // User Players
    CLock       mUsersLock;

    CreatureSet mCreatures; // others
    CLock       mCreaturesLock;
};
#endif