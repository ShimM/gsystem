#pragma once

enum IO_TYPE {
    IO_READ,
    IO_WRITE,
    IO_ACCEPT,
};

typedef struct _OVERLAPPED_EX
{
    OVERLAPPED	overlapped;
    IO_TYPE     ioType;
    PVOID       object;
} OVERLAPPED_EX;

class CIocp
{
public:
	CIocp(DWORD n = 0);
	BOOL Startup(VOID);
	BOOL Cleanup(VOID);

    BOOL RegisterSocket(SOCKET, ULONG_PTR);
    VOID WorkerThread(VOID);

    virtual VOID OnConnected(PVOID object) = 0;
    virtual VOID OnDisconnected(PVOID object) = 0;
    virtual VOID OnRead(PVOID object, DWORD dataLength) = 0;
    virtual VOID OnWrite(PVOID object, DWORD dataLength) = 0;

private:
	HANDLE	mIocpHandle;
	int		mThreadCount;
	HANDLE	mStartupEvent;
};
