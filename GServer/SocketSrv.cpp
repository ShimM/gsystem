﻿#include "stdafx.h"
#include <Ws2tcpip.h>

#pragma comment(lib,"ws2_32.lib")
#pragma comment(lib,"mswsock.lib")  // AcceptEx()

CSocketSrv::CSocketSrv()
{
    ZeroMemory(&mAcceptOverlapped, sizeof(mAcceptOverlapped));
    ZeroMemory(&mReadOverlapped, sizeof(mReadOverlapped));
    ZeroMemory(&mWriteOverlapped, sizeof(mWriteOverlapped));
    ZeroMemory(&mReadBuffer, sizeof(mReadBuffer));

    mSocket = NULL;
    mAcceptOverlapped.ioType = IO_ACCEPT;
    mReadOverlapped.ioType = IO_READ;
    mWriteOverlapped.ioType = IO_WRITE;

    mAcceptOverlapped.object = this;
    mReadOverlapped.object = this;
    mWriteOverlapped.object = this;
}

BOOL CSocketSrv::Startup(VOID)
{
    if (mSocket)
        return FALSE;

    ZeroMemory(mReadBuffer, sizeof(mReadBuffer));
    mReadEnd = READ_BUFFER_LENGTH;
    mReadHead = mReadTail = 0;  // 현재 ReadBuffer에서 유효한 범위

    ZeroMemory(mWriteBuffer, sizeof(mWriteBuffer));
    mWriteEnd = WRITE_BUFFER_LENGTH;
    mWriteHead = mWriteTail = 0;  // 현재 WriteBuffer에서 유효한 범위

    return TRUE;
}

BOOL CSocketSrv::Cleanup(VOID)
{
    if (!mSocket)
        return FALSE;

    shutdown(mSocket, SD_BOTH);
    closesocket(mSocket);
    mSocket = NULL;

    return TRUE;
}

BOOL CSocketSrv::Bind(VOID)
{
    if (mSocket)
        return FALSE;

    mSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);
    if (mSocket == INVALID_SOCKET)
        return FALSE; 

    BOOL ReuseAddr = TRUE;
    setsockopt(mSocket, SOL_SOCKET, SO_REUSEADDR, (const char *)&ReuseAddr, sizeof(ReuseAddr)); 

    // BOOL NoDelay = TRUE;
    // setsockopt(mSocket, IPPROTO_TCP, TCP_NODELAY, (const char *)&NoDelay, sizeof(NoDelay)); 

    return TRUE;
}

BOOL CSocketSrv::Listen(WORD port, INT backlog)
{
    if (port <= 0 || backlog <= 0)
        return FALSE;

    if (!mSocket)
        return FALSE;

    SOCKADDR_IN sockInfo;
    sockInfo.sin_family = AF_INET;
    sockInfo.sin_port = htons(port);
    sockInfo.sin_addr.S_un.S_addr  = htonl(INADDR_ANY);
    
    if (bind(mSocket, (struct sockaddr *) &sockInfo, sizeof(SOCKADDR_IN)) == SOCKET_ERROR) {
        Cleanup();
        return FALSE;
    }

    if (listen(mSocket, backlog) == SOCKET_ERROR) {
        Cleanup();
        return FALSE;
    }

    LINGER linger;
    linger.l_onoff  = 1;
    linger.l_linger = 0;

    if (setsockopt(mSocket, SOL_SOCKET, SO_LINGER, (char *)&linger, sizeof(LINGER)) == SOCKET_ERROR) {
        Cleanup();
        return FALSE;
    }

    return TRUE;
}

BOOL CSocketSrv::Connect(PCWSTR address, WORD port)
{
    if (!address || port <= 0)
        return FALSE;

    if (!mSocket)
        return FALSE;

    SOCKADDR_IN remoteAddrInfo;

    remoteAddrInfo.sin_family = AF_INET;
    remoteAddrInfo.sin_port = htons(port);
    InetPton(AF_INET, address, &remoteAddrInfo.sin_addr.S_un.S_addr);
    
    if (WSAConnect(mSocket, (LPSOCKADDR)&remoteAddrInfo, sizeof(SOCKADDR_IN), NULL, NULL, NULL, NULL) == SOCKET_ERROR) {
        if (WSAGetLastError() != WSAEWOULDBLOCK) {
            Cleanup();
            return FALSE;
        }
    }
    return TRUE;
}

BOOL CSocketSrv::Accept(SOCKET listenSocket)
{
    if (!listenSocket)
        return FALSE;

    if (mSocket)
        return FALSE;

    mSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, WSA_FLAG_OVERLAPPED);

    if (mSocket == INVALID_SOCKET) {
        Cleanup();
        return FALSE;
    }

    // BOOL NoDelay = TRUE;
    // setsockopt(mSocket, IPPROTO_TCP, TCP_NODELAY, (const char *)&NoDelay, sizeof(NoDelay)); 

    if (!AcceptEx(listenSocket,
        mSocket,
        mReadBuffer,
        0, // 여기서는 읽지 않는다
        sizeof(sockaddr_in) + 16,
        sizeof(sockaddr_in) + 16,
        NULL,
        &mAcceptOverlapped.overlapped)) {

        if (WSAGetLastError() != ERROR_IO_PENDING && WSAGetLastError() != WSAEWOULDBLOCK) {
            Cleanup();
            return FALSE;
        }
    }
    return TRUE;
}

SOCKET CSocketSrv::GetSocket(VOID)
{
    return mSocket;
}