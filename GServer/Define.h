#pragma once

// CONSTANT

#define DEFAULT_PORT        80      // 클라이언트가 접속하는 포트

#define MAX_SOCKET      (10000)     // 나중에 확장할 수 있으나 일단 10,001으로 하자. +1하여 accept reserve.

const WORD MAX_THREAD_COUNT = 256;  // 최대 실행 가능한 쓰레드 수. 자료구조의 크기 - call stack, thread tick, Tasklet

// 아래 두 개는 (Release 모드에서도) 계속 쓰더라도 큰 부담은 없을 듯

// Deadlock 발생시에 call stack trace.
#define DEADLOCK_TRACE

// REF_CHECK 사용한 AddRef checker. 
#define REF_DEBUG

#ifndef REF_DEBUG   // 정의하지 않으면 모두 날린다
#define AddRef(n)   AddRef()
#define Release(n)  Release()
#endif

// 대문자로 시작하는 Assert는 항상 (Debug/Release) 사용한다. 코드는 assert.h 와 동일하다.
#define DO_ASSERT

#ifdef DO_ASSERT
extern "C" {
    _CRTIMP void __cdecl _wassert(_In_z_ const wchar_t * _Message, _In_z_ const wchar_t *_File, _In_ unsigned _Line);
}
#define Assert(_Expression) \
        (void)( (!!(_Expression)) || (_wassert(_CRT_WIDE(#_Expression), _CRT_WIDE(__FILE__), __LINE__), 0) )
#else
#define Assert(expr)            ((void)0)  
#endif

// 일단 다음을 사용한다
#define super __super
