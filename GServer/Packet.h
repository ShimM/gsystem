#pragma once

class CPacket {
public:
    CPacket();
    static BOOL ProcessPacket(BYTE *body, WORD length, CSocket *socket);
};