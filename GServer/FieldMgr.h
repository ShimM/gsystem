#pragma once

typedef stdext::hash_map<DWORD, CField *> FieldMap;

class CFieldManager {
public:
    CFieldManager();
    ~CFieldManager();
    BOOL Startup();
    BOOL Cleanup();
    //BOOL Initialize();

    BOOL CreateField(WORD typeId, CField **pField);    // templateId가 같은 field를 여러 개 생성할 수 있다 (instance 경우)
    BOOL CloneField(CField *source, CField **pField);

    int GetFieldIds(DWORD *numbers);
    BOOL Exist(DWORD fieldId);
    CField *GetField(DWORD fieldId);

private:
    FieldMap mFields;
    CLock    mFieldsLock;
};

extern CFieldManager *gFieldManager;


