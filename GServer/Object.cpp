#include "stdafx.h"

CRefObject::CRefObject()
{
    mRefCount = 1;  // must be 1!!
#ifdef REF_DEBUG
    mRefCheck = 1;
#endif
}

CRefObject::~CRefObject()
{
    Release(1);
}

// AddRef와 Release의 짝이 맞아야 한다
BOOL CRefObject::AddRef(int refCheck)
{
    InterlockedIncrement(&mRefCount);

#ifdef REF_DEBUG
    InterlockedAdd64(&mRefCheck, refCheck);
#endif

    return TRUE;
}

// REF_CHECK 값은 0x100, 0x1000, 0x10000과 같이 4비트씩 준다.
// 64비트값을 사용하므로, 16자리까지 가능하다.
// 0x1은 디폴트 타입에 대해 할당한다. 디폴트 타입의 경우에 많이 사용되므로, 0x10은 비워둔다.

BOOL CRefObject::Release(int refCheck)
{
#ifndef REF_DEBUG
    if (InterlockedDecrement(&mRefCount) == 0) {
        delete this;   
    }
#else
    InterlockedAdd64(&mRefCheck, -refCheck);

    if (InterlockedDecrement(&mRefCount) == 0) {
        if (mRefCheck) {
            wchar_t str[256];
            wsprintf(str, L"mRefCheck = %X", mRefCheck);
            MessageBox(NULL, str, NULL, NULL);
            return FALSE;   // 이 경우에 delete를 건너뛰고 계속한다. (Log를 남기는 것이 좋을까?)
            //__debugbreak(); // 일단 break. 
        }
        delete this;    // 혹은 이 object들에 대해 별도로 관리하면서 모니터링 하거나?
    }
#endif
    return TRUE;
}

#ifndef _DEBUG
void * CRefObject::operator new(size_t size)
{
    // alloc from pool
    return malloc(size);
}

void CRefObject::operator delete (void *p)
{
    // return to pool
    free(p);
}
#endif

////////////////////////////////////////////////////////////////////////////////

BOOL CObject::AddTask(WORD message, PVOID param)
{
    // 명시적으로 serial 호출하는 방법 외에, AddTask에서 message에 의한 구분
    if (message & SERIAL_MSG_BEGIN)   
        AddTaskSerial(message, param);
    else
        gGlobalTaskQueue->AddTask(this, message, param);

    return TRUE;
}

BOOL CObject::AddTaskMain(WORD message, PVOID param)
{
    gMainTaskQueue->AddTask(this, message, param);
    // AddTask가 정상 동작하지 않아서 AddTimer(0, this, message, param)로 잠시 교체했으나,
    // 현재 Task를 실행 중인 쓰레드에서 AddTask를 호출하는 경우에 CriticalSection의 re-enter 기능으로 오동작. (가능해도 문제)
    // SRWLock으로 교체하여 해결함

    return TRUE;
}

BOOL CObject::AddTask(ITaskQueue *queue, WORD message, PVOID param)
{
    if (!queue)
        return FALSE;

    queue->AddTask(this, message, param);

    return TRUE;
}

BOOL CObject::AddTasklet(WORD message, PVOID param)
{
    ::AddTasklet(this, message, param);

    return TRUE;
}

BOOL CObject::AddTimer(DWORD delta, WORD message, PVOID param)
{
    // 명시적으로 serial 호출하는 방법 외에, AddTask에서 message에 의한 구분
    if (message & SERIAL_MSG_BEGIN)
        AddTimerSerial(delta, message, param);
    else
        gGlobalTaskQueue->AddTimer(delta, this, message, param);

    return TRUE;
}

BOOL CObject::AddTimerMain(DWORD delta, WORD message, PVOID param)
{
    gMainTaskQueue->AddTimer(delta, this, message, param);

    return TRUE;
}

BOOL CObject::AddTimer(ITaskQueue *queue, DWORD delta, WORD message, PVOID param)
{
    if (!queue)
        return FALSE;

    queue->AddTimer(delta, this, message, param);
    return TRUE;
}

BOOL CObject::AddTaskSerial(WORD message, PVOID param)
{
    DWORD hash = HashCode();
    CSerialTaskQueue *queue = gSerialTaskQueueArray[hash & SERIAL_QUEUE_MASK];

    if (!queue)
        return FALSE;

    queue->AddTask(this, message, param);

    return TRUE;
}

BOOL CObject::AddTimerSerial(DWORD delta, WORD message, PVOID param)
{
    DWORD hash = HashCode();
	WORD queueSelect = WORD(hash & SERIAL_QUEUE_MASK);
    CSerialTaskQueue *queue = gSerialTaskQueueArray[queueSelect];

    if (!queue)
        return FALSE;

    queue->AddTimer(delta, this, message, param);

    return TRUE;
}

