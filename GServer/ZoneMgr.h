#pragma once
#if 0
typedef stdext::hash_map<DWORD, CZone *> ZoneMap;

class CZoneManager {
public:
    CZoneManager();
    ~CZoneManager();
    BOOL Startup();
    BOOL Cleanup();
    //BOOL Initialize();

    BOOL CreateZone(WORD typeId, CZone **pZone);    // templateId가 같은 field를 여러 개 생성할 수 있다 (instance 경우)
    //BOOL CloneField(CField *source, CField **pField);

    int GetZoneIds(DWORD *numbers);
    BOOL Exist(DWORD zoneId);   // 없애고 아래 것 사용?
    CZone *GetZone(DWORD zoneId);

private:
    ZoneMap mZones;
    CLock mZonesLock;
};
#endif
//extern CZoneManager *gZoneManager;