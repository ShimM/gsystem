#include "stdafx.h"
#include <process.h>

// Task Queue는 circular queue를 사용하여, Disruptor의 방식과 비슷하게 처리한다
// Apple의 GCD와 비슷하며, 이 자체에서 여러 큐를 관리한다. 특정 큐를 지정하여 넣을 수도 있다 (1 Queue - 1 Thread)
// 실행중인 쓰레드에서도 add 가능하고, 밖에서 넣을 수도 있다. single 개념을 지키기는 어렵다
// http://www.codeproject.com/Articles/43510/Lock-Free-Single-Producer-Single-Consumer-Circular

// 이전 프로젝트에서는 AddTask에 해당하는 경우에 별도의 IOCP에 넣어서 처리했다. (IOCP당 쓰레드 1~2개) 그래도 I/O와 Job은 분리한 셈이다
// 그리고 입력으로 들어오는 object의 주소를 오른쪽 6 shift한 다음에 hash하여, object 단위로 locality를 주었다
//
// 2014.09.01: 모든 task는 task queue를 거쳐서 실행된다. queue마다 하나의 thread가 있는 것은 외부에서 명시적으로 queue를 사용할 수 있게 하므로 좋다
// map id에 의해서 해싱하면 로컬리티가 생긴다. 그러나, map id별로 하나의 thread가 대응되어 있으므로, 결국 map 단위로 serialize 되는 문제가 있다
// https://speakerdeck.com/trent/pyparallel-how-we-removed-the-gil-and-exploited-all-cores
// IOCP thread affinity : 같은 클라이언트는 항상 같은 thread가 처리해야 한다 (Map 단위가 아니라 클라이언트 접속 단위로 한다)

// 09.03: 위의 방법은 한 큐의 task가 블럭될 때 밀리는 문제가 있다. IOCP하나에 n개의 쓰레드가 물려있는 것을 기본 단위로 하고, 이것을 여러 개 둘 수 있도록 한다.
// 그리고, 타이머를 비롯해서, 큐에 넣을 때 큐를 선택할 수 있도록 한다.

// Concurrency Runtime
// http://msdn.microsoft.com/en-us/library/dd504870.aspx
// Task Scheduler(Concurrency Runtime)
// http://msdn.microsoft.com/en-us/library/dd984036.aspx

#define REF_CHECK 0x1000

////////////////////////////////////////////////////////////////////////////////

CBaseTaskQueue    *gGlobalTaskQueue = NULL;

CBaseTaskQueue    *gMainTaskQueue = NULL;

CSerialTaskQueue  *gSerialTaskQueueArray[SERIAL_QUEUE_COUNT] = { NULL, };

__declspec(thread) int gThreadId;

static DWORD      sWorkerThreadCount = 0;

int GetWorkerThreadCount() { return sWorkerThreadCount; }

static CSerialTaskQueue *sSerialTaskQueue[65536] = { 0, };
static DWORD sSerialTaskQueueIndex = 0;

////////////////////////////////////////////////////////////////////////////////
// Tasklet: thread 내에서 더하고, schedule 실행 끝에서 계속 실행한다
// thread 안에서 실행하므로, Interlocked 없이 처리한다

class CTaskletStack {
public:
    CTaskletStack();
    
    BOOL AddTask(POBJECT object, WORD message, PVOID param);
    BOOL RunTasks(BOOL isSerial);

private:
    MessageItem stack[TASKLET_STACK_SIZE];
    DWORD mTop;
};

CTaskletStack gTaskletStack[MAX_THREAD_COUNT];

CTaskletStack::CTaskletStack()
{
    mTop = 0;
}

BOOL CTaskletStack::AddTask(POBJECT object, WORD message, PVOID param)
{
    int pos = mTop++;

    Assert(mTop != TASKLET_STACK_SIZE);

    object->AddRef(REF_CHECK); // !!!

    stack[pos].object = object;
    stack[pos].message = message;
    stack[pos].param = param;

    return TRUE;
}

BOOL CTaskletStack::RunTasks(BOOL isSerial)
{
    if (mTop == 0)
        return FALSE;

    while (1) {
        int pos = --mTop;

        POBJECT object = stack[pos].object;
        WORD message = stack[pos].message;
        PVOID param = stack[pos].param;

        if (object) {
            // 이 둘은 어차피 한 쓰레드 안에서 수행되므로 순차적으로 실행된다. (AddTask가 아니다!)
            // 여기서 구분하는 이유는 단지 RunSerial에서 불렀으면 RunSerial 쪽으로 호출해 주기 위함이다.
            if (isSerial)
                object->RunSerial(message, param);
            else
                object->Run(message, param);

            object->Release(REF_CHECK); // !!!
        }
        if (mTop == 0)
            break;
    }
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

static unsigned __stdcall GeneralThreadFunction(LPVOID lpParam);

// n은 생성할 쓰레드 수. 0이면 시스템의 프로세서 수만큼 생성한다.
// n이 2의 제곱이 아니면, 이보다 작은 2의 제곱수의 쓰레드를 생성한다.
CBaseTaskQueue::CBaseTaskQueue(DWORD n)
{
    for (int i = 0; i < IOCP_PORT_MAX; i++) {
        mIocpHandles[i] = 0;
    }

    if (n == 0)
        n = GetNumberOfProcessors();

    // n이 2의 제곱이 아니면, n보다 작은 2의 제곱인 수를 구한다.
    while (n & (n - 1))
        n = n & (n - 1);

    mThreadCount = n;

    mIocpPortCount = mThreadCount / THREAD_PER_PORT;
    if (mIocpPortCount < 1)
        mIocpPortCount = 1;
    if (mIocpPortCount > IOCP_PORT_MAX)
        mIocpPortCount = IOCP_PORT_MAX;

    mIocpPortMask = mIocpPortCount - 1;

    Startup();
}

CBaseTaskQueue::~CBaseTaskQueue()
{
    Cleanup();
}

struct ThreadArg {
    CBaseTaskQueue *ptr;
    int            threadId;
};

BOOL CBaseTaskQueue::Startup()
{
    for (int i = 0; i < mIocpPortCount; i++) {
        mIocpHandles[i] = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, THREAD_PER_PORT);	// 각 port마다 담당하는 thread 수.
        if (!mIocpHandles[i])
            return FALSE;
    }

    mStartupEvent = CreateEvent(0, FALSE, FALSE, 0);    // auto-reset
    if (mStartupEvent == NULL) {
        Cleanup();
        return FALSE;
    }

    ThreadArg argList[MAX_THREAD_COUNT];

    // single thread queue를 만들 때는 1개만 생성. 그렇지 않을 때는 2배를 생성하자.
    int count = (mThreadCount == 1) ? 1 : (int)(mThreadCount * 2);

    for (int i = 0; i < count; i++) {
        argList[i].ptr = this;
        argList[i].threadId = sWorkerThreadCount++;   // 로컬 변수로 재활용하면 안됨(순차 실행하면 관계없나?)
    }
    for (int i = 0; i < count; i++) {
        _beginthreadex(NULL, 0, ::GeneralThreadFunction, &argList[i], 0, NULL);
        WaitForSingleObject(mStartupEvent, INFINITE);   // 순차 실행
    }

    for (int i = 0; i < TIMER_QUEUE_COUNT; i++) {
        mShortTimerQueue[i].Startup(this);
    }
    mLongTimerQueue.Startup(this);

    return TRUE;
}

BOOL CBaseTaskQueue::Cleanup(VOID)
{
    for (int i = 0; i < mIocpPortCount; i++) {
        if (mIocpHandles[i])
            CloseHandle(mIocpHandles[i]);
    }
    if (mStartupEvent)
        CloseHandle(mStartupEvent);

    for (int i = 0; i < TIMER_QUEUE_COUNT; i++) {
        mShortTimerQueue[i].Cleanup();
    }
    mLongTimerQueue.Cleanup();

    return TRUE;
}

BOOL CBaseTaskQueue::AddTask_(POBJECT object, DWORD lmessage, PVOID param)
{
    object->AddRef(REF_CHECK);

    const int mask = mIocpPortMask;
    PostQueuedCompletionStatus(
        mIocpHandles[mask ? (object->HashCode() & mask) : 0],
        lmessage,
        (ULONG_PTR)object,
        (LPOVERLAPPED)param);

    return TRUE;
}

BOOL CBaseTaskQueue::AddTask(POBJECT object, WORD message, PVOID param)
{
    return AddTask_(object, message, param);
}

BOOL AddTasklet(POBJECT object, WORD message, PVOID param)
{
    return gTaskletStack[gThreadId].AddTask(object, message, param);
}

BOOL CBaseTaskQueue::AddTimer(DWORD delta, POBJECT object, WORD message, PVOID param)
{
    if (delta < 30000)
        mShortTimerQueue[object->HashCode() & TIMER_QUEUE_COUNT]
        .AddTimer(object, delta, message, param);
    else
        mLongTimerQueue.AddTimer(object, delta, message, param);
    return TRUE;
}

unsigned _stdcall GeneralThreadFunction(LPVOID param)
{
    if (!param)
        return -1;

    ThreadArg *arg = (ThreadArg *)param;

    (arg->ptr)->WorkerThread(arg->threadId);
    return 0;
}

BOOL CBaseTaskQueue::WorkerThread(int threadId)
{
    SetEvent(mStartupEvent);
    gThreadId = threadId;

    BOOL success = FALSE;
    POBJECT object = NULL;
    PVOID param = NULL;
    DWORD lmessage = 0;

    while (TRUE) {
        gThreadGetTickCount[threadId] = 0;   // GQCS에서 기다림 표시

        success = GetQueuedCompletionStatus(
            mIocpHandles[threadId & mIocpPortMask],
            &lmessage,
            (PULONG_PTR)&object,
            (LPOVERLAPPED *)&param,
            INFINITE);  // 원래 INFINITE 였으나, 데드락 검사를 위해 1초로 수정 => 다시 INFINITE

        if (gShutdownState)
            return FALSE;

        // 데드락 체크는 worker에서 자신의 딜레이를 검사할 수 없다. 메인쓰레드로 옮긴다.
        // 여기서는 1초에 한번 시간을 기록하여, 쓰레드가 살아있다고 알려준다.
        gThreadGetTickCount[threadId] = GetTickCountNow();

        if (!success)
            continue;

        WORD message = (lmessage & 0xFFFF);
        int serialQueueIndex = (lmessage >> 16);

        // 콜백 측면에서 Run과 RunSerial의 구분은 없다. Serial은 호출에서 정한다. 콜백에서 구분할 것은 아니다!
        // => 작성된 콜백에 대해서 가이드라인에 따라 작성된 것인지 확인하기 위해서는 구분이 필요하다!
        if (serialQueueIndex) {     // > 0, if from Serial Queue
            if (object) {
                WORD ret = object->RunSerial(message, param);
                gTaskletStack[gThreadId].RunTasks(TRUE);    // tasklet을 RunOut보다 먼저 처리 (Serial은 안에서)

                sSerialTaskQueue[serialQueueIndex]->Dispatch(object);    // Serial의 다음 항목을 실행하도록 요청

                if (ret) {
                    object->RunOut(ret);    // RunOut은 serial 밖에서 실행한다
                }
                object->Release(REF_CHECK);
            }
        }
        else {
            if (object) {
                WORD ret = object->Run(message, param);
                gTaskletStack[gThreadId].RunTasks(FALSE);    // tasklet을 RunOut보다 먼저 처리

                // RunOut은 serial, parallel의 짝을 맞추기 위해 여기서도 호출한다
                // 어떤 case를 serial에서 parallel로 옮길 때, 이 부분이 없으면 코드를 많이 고쳐야 할 수 있다.
                if (ret) {
                    object->RunOut(ret);
                }
                object->Release(REF_CHECK);
            }
        }
    }
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

CCircularQueue::CCircularQueue()
{
    mHead = mTail = 0;
}

VOID CCircularQueue::AddTask(POBJECT object, WORD message, PVOID param)
{
    // 저장되는 곳이 항상 mHead <= x < mTail 이 되도록 유지한다
    // mHead, mTail은 둘 다 증가만 한다. 이 두 값에 대해서 나머지 % 얻지 않는다

    //int pos = (mTail++) % CIRCULAR_QUEUE_MASK;
    int pos = (InterlockedIncrement(&mTail) - 1) & CIRCULAR_QUEUE_MASK;

    Assert(mTail % CIRCULAR_QUEUE_MASK != mHead % CIRCULAR_QUEUE_MASK);
    object->AddRef(REF_CHECK);

    queue[pos].object = object;
    queue[pos].message = message;
    queue[pos].param = param;
}

unsigned CCircularQueue::Length()
{
    return (mTail - mHead);
}

BOOL CCircularQueue::GetTask(POBJECT *pObject, WORD *pMessage, PVOID *pParam)
{
    if (mHead == mTail) // empty
        return FALSE;

    //int pos = (mHead++) % CIRCULAR_QUEUE_MASK;
    int pos = (InterlockedIncrement(&mHead) - 1) & CIRCULAR_QUEUE_MASK;

    *pObject = queue[pos].object;
    *pMessage = queue[pos].message;
    *pParam = queue[pos].param;

    (*pObject)->Release(REF_CHECK);

    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

CSerialTaskQueue *GetSerialTaskQueue(WORD index)
{
    if (index == 0 || index > sSerialTaskQueueIndex)
        return NULL;

    return sSerialTaskQueue[index];
}

CSerialTaskQueue::CSerialTaskQueue(CBaseTaskQueue *baseQueue)
{
    Startup();

    mBaseQueue = NULL;
    if (baseQueue)
        Initialize(baseQueue);
}

CSerialTaskQueue::~CSerialTaskQueue()
{
    Cleanup();
}

DWORD CSerialTaskQueue::Initialize(CBaseTaskQueue *baseQueue)
{
    mBaseQueue = baseQueue;

    mQueueIndex = InterlockedIncrement(&sSerialTaskQueueIndex); // begin from 1
    sSerialTaskQueue[mQueueIndex] = this;

    return mQueueIndex;
}

BOOL CSerialTaskQueue::Startup()
{
    mTimerQueue.Startup(this);

    return TRUE;
}

BOOL CSerialTaskQueue::Cleanup()
{
    mTimerQueue.Cleanup();

    return TRUE;
}

BOOL CSerialTaskQueue::AddTimer(DWORD time, POBJECT object, WORD message, PVOID param)
{
    mTimerQueue.AddTimer(object, time, message, param);
    return TRUE;
}

BOOL CSerialTaskQueue::AddTask(POBJECT object, WORD message, PVOID param)
{
    if (!mBaseQueue)
        return FALSE;

    DWORD lmessage = (mQueueIndex << 16) | message; // 상위 16비트에 Index 값 세팅

    // 여기서 바로 들어가면 안되고, 다른 쓰레드가 실행 중이면 큐에 넣고 리턴한다.
#ifdef OBJECT_SERIALIZE
    if (object->mSerialLock.TryLockExclusive()) {
#else
    if (mSerialLock.TryLockExclusive()) {
#endif
        assert(lmessage >> 16);
        mBaseQueue->AddTask_(object, lmessage, param);
    }
    else {
        object->AddRef(REF_CHECK);

#ifdef OBJECT_SERIALIZE
        object->mIocpQueue.Post(object, lmessage, param);
#else
        mIocpQueue.Post(object, lmessage, param);
        //PostQueuedCompletionStatus(             // (IOCP)
        //    mIocpHandle,
        //    lmessage,
        //    (ULONG_PTR)object,
        //    (LPOVERLAPPED)param);
#endif
    }

    return TRUE;
}

BOOL CSerialTaskQueue::Dispatch(POBJECT runObject)
{
    // 큐에 뭐가 있으면, '한 개' 가져와서 AddTask를 불러서 실행한다
    // 가져올 것이 없으면 '실행중인 것이 없음' flag를 설정한다.

    BOOL success = FALSE;

    POBJECT object = NULL;
    PVOID param    = NULL;
    DWORD lmessage = 0;
    WORD message   = 0;

#ifdef OBJECT_SERIALIZE
    success = runObject->mIocpQueue.Get(&object, &lmessage, &param, 0);
#else
    success = mIocpQueue.Get(&object, &lmessage, &param, 0);
    //success = GetQueuedCompletionStatus(
    //    mIocpHandle,
    //    &lmessage,
    //    (PULONG_PTR)&object,
    //    (LPOVERLAPPED *)&param,
    //    0); // 기다리지 않는다
#endif

    if (gShutdownState)
        return FALSE;

    if (!success) {   // empty queue
#ifdef OBJECT_SERIALIZE
        runObject->mSerialLock.UnlockExclusive();
#else
        mSerialLock.UnlockExclusive();
#endif
    }
    else {  // 계속 lock 상태를 유지한다
        assert(lmessage >> 16);
        mBaseQueue->AddTask_(object, lmessage, param);  // upper queue에서 가져온 것을 base queue에 넣는다
        object->Release(REF_CHECK);
    }
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

//CTaskQueueGroup gTaskQueue; // multiples
//CTaskQueue      gSerialTaskQueue;   // single queue - single thread. inserted to gTaskQueue
#if 0
static unsigned __stdcall ThreadFunction(LPVOID lpParam);

CTaskQueue::CTaskQueue()
{
    mHead = mTail = 0;
    mEvent = 0;

    Startup();
}

CTaskQueue::~CTaskQueue()
{
}

void CTaskQueue::Startup()
{
    CreateEvent(0, FALSE, FALSE, 0); // auto-reset
    _beginthreadex(NULL, 0, ::ThreadFunction, this, 0, NULL);
}

BOOL CTaskQueue::AddTask(POBJECT object, WORD message, PVOID param)
{
    int pos = (InterlockedIncrement(&mTail) - 1) & CIRCULAR_QUEUE_MASK;

    Assert(mTail % CIRCULAR_QUEUE_MASK != mHead % CIRCULAR_QUEUE_MASK);

    object->AddRef(REF_CHECK);

    queue[pos].object = object;
    queue[pos].param = param;
    queue[pos].message = message;

    SetEvent(mEvent);

    return TRUE;
}

unsigned CTaskQueue::Length()
{
    return (mTail - mHead);
}

BOOL CTaskQueue::Thread()
{
    for (;;) {
        WaitForSingleObject(mEvent, INFINITE);

        if (gShutdownState)    // Task Queue에서 검사
            return FALSE;

        while (mHead != mTail) {
            int pos = (InterlockedIncrement(&mHead) - 1) & CIRCULAR_QUEUE_MASK;

            POBJECT object = queue[pos].object;
            WORD message = queue[pos].message;
            PVOID param = queue[pos].param;

            if (object) {
                object->Run(message, param);
                object->Release(REF_CHECK);
            }
        }
    }
}

unsigned _stdcall ThreadFunction(LPVOID ptr)
{
    if (!ptr)
        return -1;

    ((CTaskQueue *)ptr)->Thread();
    return 0;
}

/////

CTaskQueueGroup::CTaskQueueGroup()
{
    mQueueCount = GetNumberOfProcessors();
    mQueue = new CTaskQueue[mQueueCount];
}

CTaskQueueGroup::~CTaskQueueGroup()
{
    if (mQueue)
        delete[] mQueue;
}

int CTaskQueueGroup::GetNumQueue(VOID)
{
    return mQueueCount;
}

BOOL CTaskQueueGroup::AddTask(POBJECT object, WORD message, PVOID param)
{
    unsigned queue = (unsigned)rand() % mQueueCount;

    mQueue[queue].AddTask(object, message, param);

    return TRUE;
}

BOOL CTaskQueueGroup::AddTaskSelect(POBJECT object, WORD message, PVOID param, unsigned select)
{
    unsigned queue = (unsigned)select % mQueueCount;

    mQueue[queue].AddTask(object, message, param);

    return TRUE;
}
#endif
