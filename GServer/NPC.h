#pragma once

class CNpc : public CGameObject {
public:
    CNpc();
    virtual ~CNpc() override;

protected:
    WORD Run(WORD message, PVOID param) override;
    WORD RunSerial(WORD message, PVOID param) override;
    virtual VOID  RunOut(WORD message) override;
};

extern DWORD gNpcCount;
extern DWORD gNpcRunCount; // Run ȣ�� Ƚ��