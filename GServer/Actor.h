// Actor의 서브클래스들은 모두 public method를 가질 수 없다.
// 단 예외적으로 내부 상태를 바꾸지 않는, const인 member function은 가능하다.
// Actor의 내부 상태를 변경하는 모든 작업은 Run 안에서만 해야 한다. (자신에 대해서 lock이 걸린 상태로 보면 된다)

#if 0
class CActor : public CObject {
//    friend class CBaseTaskQueue;
//    friend class CTaskletQueue;
public:
    // object id의 해시값에 따라 serial queue를 선택한다
    BOOL AddTask(WORD message, PVOID param);            
    BOOL AddTimer(DWORD delta, WORD message, PVOID param);
};

//typedef CActor *PACOTR;
#endif