// (c) Francois Guibert, www.frozax.com (@Frozax)
// http://www.frozax.com/blog/2012/10/simple-useful-finite-state-machine-fsm-c/

#pragma once

template<typename T>
class FSM
{
public:
    FSM() : _time_in_cur_state(0.0f), _cur_state(-1)
    {
    }

    virtual void BeginState(T state) {}
    virtual void UpdateState(T state) {}
    virtual void EndState(T state) {}

    void SetState(T state)
    {
        EndState((T)_cur_state);
        _cur_state = state;
        _time_in_cur_state = 0.0f;
        BeginState((T)_cur_state);
    }

    void UpdateFSM(float delta_time)
    {
        if (_cur_state != -1)
        {
            _time_in_cur_state += delta_time;
            UpdateState((T)_cur_state);
        }
    }

    float GetTimeInCurState() { return _time_in_cur_state; }
    T GetState() { return (T)_cur_state; }

private:
    float _time_in_cur_state;
    int _cur_state;
};


// First create the enum you will use for the states, such as :

enum EState
{
    STT_OFF = -1, // optional, -1 is the initial state of the fsm
    STT_WALK,
    STT_RUN,
    STT_STOP,
    STT_EAT
};

// And inherit from the class fgFSM :


class ObjectUsingFSM : public fgFSM<EState>
{
public: 
    // ...
    void UpdateState(EState t);
    void BeginState(EState t);
    void EndState(EState t);
    // ...
};

