#pragma once 

// All Game Object's Root Class

// "게임 프로그래머를 위한 C++"에서, GetUnitType() p.325
// 게임 프로그래밍의 핵심 법칙. 가상 함수의 잘못된 사용. p39
// 가상함수를 사용하지 않고, 그냥 변수 값을 돌려준다. 

enum {
    TYPE_OBJECT = 0,    // 굳이 값을 주지 않아도 될 듯?
    TYPE_PLAYER,
    TYPE_NPC
};

// GameObject - game object on field. map 관련 데이터도 여기 저장된다.

// PC와 NPC의 ID는 분리해야 할까?
static DWORD sObjectSequenceId = 0; // 42억까지만 가능하다.

class CGameObject : public CObject {
public:

    CGameObject() { 
        mType = TYPE_OBJECT; 
        mId = InterlockedIncrement(&sObjectSequenceId);
        Assert(mId);
        //Initialize();
    }

    WORD GetType() const {
        return mType;
    }

    DWORD GetId() const {
        return mId;
    }

protected:
    WORD        mType;
    WORD        mAttr;
    DWORD       mFieldId; // object가 속한 Field의 id

    DWORD       mId;      // User, NPC 공통으로 사용하는 id. (구분 bit?)

private:
    POINT       mPos;
};

// GameObject의 id 체계 - 9자리를 가지고 어떻게 할당할 것인가? 굳이 필요하긴 한건가?
