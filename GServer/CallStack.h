#pragma once

extern VOID DumpCallStack();

#ifdef DEADLOCK_TRACE

const DWORD STACK_SIZE = 128;
const DWORD STACK_MASK = STACK_SIZE - 1;

struct StackItem {
    const char    *file;
    int            line;
    const char    *func;
};

class CCallStack {
public:
    CCallStack();
    BOOL Push(const char *file, int line, const char *func);
    BOOL Pop();
    VOID Dump(FILE *fp);

private:
    StackItem   stack[STACK_SIZE];
    DWORD       mHead, mTail;
};

extern CCallStack gCallStack[MAX_THREAD_COUNT];

// ENTRY 정의: 함수 시작 부분에 사용
#define ENTRY EntryRaiiObject TOKENPASTE(_entry, __LINE__) (__FILE__, __LINE__, __FUNCTION__ );

struct EntryRaiiObject {
    EntryRaiiObject(const char *file, int line, const char *func) { gCallStack[gThreadId].Push(file, line, func); }
    ~EntryRaiiObject() { gCallStack[gThreadId].Pop(); }
};
#else
#define ENTRY
#endif // DEADLOCK_TRACE
