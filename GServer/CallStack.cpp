#include "stdafx.h"
#include <time.h>

// 이것이 주기적으로 업데이트 되지 않으면 데드락으로 의심한다
DWORD gThreadGetTickCount[MAX_THREAD_COUNT];    // GetTickCountNow()로 채운다

#ifdef DEADLOCK_TRACE
// call stack
CCallStack gCallStack[MAX_THREAD_COUNT];

CCallStack::CCallStack()
{
    mHead = mTail = 0;
}

BOOL CCallStack::Push(const char *file, int line, const char *func)
{
    // 저장되는 곳이 항상 mHead <= x < mTail 이 되도록 유지한다
    int pos = (mTail++) & STACK_MASK;
    if (mTail % STACK_MASK == mHead % STACK_MASK) {
        mHead++;
    }
    stack[pos].file = file;
    stack[pos].line = line;
    stack[pos].func = func;

    return TRUE;
}

BOOL CCallStack::Pop()
{
    if (mHead == mTail) { // 비어있음
        return FALSE;
    }
    mTail--;

    return TRUE;
}

static void setspace(char *s, int n)
{
    int i;
    for (i = 0; i < n; i++)
        s[i] = ' ';
    s[i] = '\0';
}

VOID CCallStack::Dump(FILE *fp)
{
    char buf[128], space[128];

    for (DWORD h = mHead; h != mTail; h++) {
        int i = h % STACK_MASK;
        sprintf_s(buf, "%s:%d", stack[i].file, stack[i].line);
        setspace(space, 80 - (int)strlen(buf) - (int)strlen(stack[i].func));
        fprintf(fp, "%s%s%s\n", stack[i].func, space, buf);
    }
}

CLock dumpLock;

// CallStack은 로컬의 C:\Log에 저장한다. 폴더가 없으면 저장하지 않는다.
// 실행화일의 위치와 관계없이 프로그램이 실행되는 컴퓨터에 저장되는 방식이 좋을듯

VOID DumpCallStack()
{
    dumpLock.Lock();
    time_t rawtime;
    struct tm timeinfo;
    char buf[80];

    time(&rawtime);
    localtime_s(&timeinfo, &rawtime);
    strftime(buf, 80, "%Y-%m-%d-%H-%M-%S", &timeinfo);

    char str[256];
    sprintf_s(str, "C:\\Log\\CallStackDump-%s.txt", buf);

    FILE *fp;
    fopen_s(&fp, str, "w");
    if (!fp)
        goto error;

    fprintf(fp, "%s\n", str);
    DWORD now = GetTickCountNow();
    char delimeter[] = "--------------------------------------------------------------------------------\n";
    fprintf(fp, delimeter);

    for (int i = 0; i < GetWorkerThreadCount(); i++) {
        if (gThreadGetTickCount[i] == 0)    // in GQCS
            continue;
        if (now - gThreadGetTickCount[i] == 0)
            continue;
        fprintf(fp, "Thread #%d ... Blocked in %d msec.\n", i, now - gThreadGetTickCount[i]);
        gCallStack[i].Dump(fp);
        fprintf(fp, delimeter);
    }
    fclose(fp);

error:
    dumpLock.Unlock();
    __debugbreak(); // 데드락이 발생했을 때는 중단해야.
}
#else
VOID DumpCallStack() {}
#endif