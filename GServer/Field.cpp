#include "stdafx.h"

// 유저가 외부에서 명시적으로 볼 수 있고 선택하는 서버 단위를 World 서버라고 부르자. 
// 따라서 world 서버 안에서 다시 world를 사용하는 것은 좀 곤란하다.

// world 안에 여러 서버 프로세스로 구성되어 있으면 region 서버라고 부른다.
// region 서버는 한 리얼 머신에 있을 수도 있고, 아닐 수도 있다.

// World / Region 안에서, map 데이터를 바탕으로 하는 지역 단위는 Field로 부른다.

// map은 정적인 데이터, 길찾기 알고리즘 등, 기획과 무관한 계층으로 정의한다.

// 서버 안에 persistent field와, channel, instance 등이 존재한다
// channel과 instance는 한 map template에 대해 병렬로 존재하는 면은 같지만, static, dynamic 차이가 있다.

////////////////////////////////////////////////////////////////////////////////

// MAP
// world -> Field ? / region -> zone. (map)
// area, field, territory

// Field => Zone => Area ? (온라인 게임 네트워크 프로그래밍)

// SERVERS
// world server
// region server
// channel server
// instance server

// 지형 template 역할을 하는 - Map data? 
// Map, Map number

// int를 사용하여 4자리 template, 4자리 sequence number를 사용한다. - X
// 9자리가 주어져 있으니, 2자리는 월드 서버, 4자리는 base template, 3자리는 real number로 하면 어떨까? - X

// 가장 단순하게 2 BYTE base, 2 BYTE sequence로 한다.

//Area, Zone, Region 등 단어 사용.Map 위에서 object 움직임.
// channel, instance 
// base 

#define REF_CHECK 0x100000

static DWORD sequenceNumber = 0;

CField::CField()
{
}

CField::CField(WORD fieldType)
{
    // loading map data...

    if (!gFieldManager)
        return;

    for (;;) {
        InterlockedIncrement(&sequenceNumber);
        mFieldId = (fieldType << 16) + WORD(sequenceNumber);
        if (gFieldManager->Exist(mFieldId))
            continue;
        break;
    }

    mMap.Initialize(4096, 4096);
}

CField::~CField()
{
}

BOOL CField::PutObject(CGameObject *object)
{
    object->AddRef(REF_CHECK);

    if (object->GetType() == TYPE_PLAYER) {
        AutoLock(mUsersLock);
        mUsers.insert((CUser *)object);
    }
    else {
        AutoLock(mCreaturesLock);
        mCreatures.insert(object);
    }

    //mMap.PutObject(); // mMap의 sector (secx, secy)에다가 object를 넣는다.

    return TRUE;
}

BOOL CField::RemoveObject(CGameObject *object)
{
    // mMap에서 먼저 제거한다.

    if (object->GetType() == TYPE_PLAYER) {
        AutoLock(mUsersLock);
        auto it = mUsers.find((CUser *)object);
        if (it != mUsers.end()) {
            object->Release(REF_CHECK);
            mUsers.erase((CUser *)object);
        }
    }
    else {
        AutoLock(mCreaturesLock);
        auto it = mCreatures.find((CUser *)object);
        if (it != mCreatures.end()) {
            object->Release(REF_CHECK);
            mCreatures.erase((CUser *)object);
        }
    }
    return TRUE;
}

BOOL CField::MoveObject(CGameObject *object)
{
    // TODO:
 
    // sector가 변경되었는지 확인한 다음, 변경된 경우에 mMap에서 제거 및 추가를 진행한다.
    // 물론 이동이 가능한 경우에만 이동해야 한다.
    return TRUE;
}

BOOL CField::Broadcast(WORD message, PVOID param) {
    AutoLock(mUsersLock);

    for (auto it = mUsers.begin(); it != mUsers.end(); ++it)
        (*it)->AddTask(message, param);

    return TRUE;
}

// range check를 어떻게 하지?
BOOL CField::Broadcast(WORD message, PVOID param, Point pt, WORD range)
{
    AutoLock(mUsersLock);

    for (auto it = mUsers.begin(); it != mUsers.end(); ++it)
        (*it)->AddTask(message, param);

    return TRUE;
}

int CField::GetUserIds(std::vector<DWORD> &numbers)
{
    for (auto it = mUsers.begin(); it != mUsers.end(); ++it)
        numbers.push_back((*it)->GetId());

    return 0;
}

int CField::GetUsers(std::vector<CUser *> &users)
{
    AutoLock(mUsersLock);

    int count = 0;
    for (auto it = mUsers.begin(); it != mUsers.end(); ++it) {
        users.push_back(*it);
        //users[count]->AddRef(1);
    }

    return count;
}
