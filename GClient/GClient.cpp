// GClient.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Ws2tcpip.h>
#include <process.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

BOOL Initialize(VOID);
SOCKET Socket(VOID);
BOOL Connect(SOCKET s, PCWSTR serverAddr);

BOOL Tester(VOID);
VOID Room(VOID);
VOID User(VOID);
VOID Enter(char *p);
VOID Chat(char *p);

char message[1024];
int size;
SOCKET sock;

unsigned _stdcall ThreadFunction(LPVOID ptr);
VOID Recv_Print(VOID);

BOOL threadRun = 0;

int _tmain(int argc, _TCHAR* argv[])
{
    if (!Initialize())
        exit(1);


    sock = Socket();

    if (!Connect(sock, L"127.0.0.1"))
        exit(1);


    // LOGIN protocol
    size = EncodeLogin((BYTE*)message, LOGIN, "maro", "1234");
    *(SHORT *)message = size; // 이 역할은 서버에서 PutPacket이 한다

    if (send(sock, message, size, 0) < 0)    {
        puts("Send Login failed");
        return 1;
    }


    char buf[256];
    printf("exit for exiting program.\n");
    printf("commands: exit, test, room, user, enter, chat\n");

    for (;;) {
        gets_s(buf);
        //printf("%s\n", buf);
        if (strcmp(buf, "exit") == 0)
            break;
        else if (strcmp(buf, "test") == 0)
            Tester();

        if (!threadRun) {
            _beginthreadex(NULL, 0, ::ThreadFunction, NULL, 0, NULL);
            threadRun = 1;
        }

        if (strcmp(buf, "room") == 0)
            Room();
        else if (strcmp(buf, "user") == 0)
            User();
        else if (strncmp(buf, "enter", 5) == 0)
            Enter(buf+6);
        else if (strncmp(buf, "chat", 4) == 0)
            Chat(buf + 5);
    }
    return 0;
}

BOOL Initialize(VOID)
{
    WSADATA wsa;

    printf("\nInitialising Winsock... ");
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
    {
        printf("Failed. Error Code : %d", WSAGetLastError());
        return FALSE;
    }
    printf("Done.\n");
    return TRUE;

}

SOCKET Socket(VOID)
{

    SOCKET s;

    //Create a socket
    if ((s = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
    {
        printf("Could not create socket : %d", WSAGetLastError());
    }

    printf("Socket created.\n");

    return s;
}

BOOL Connect(SOCKET s, PCWSTR serverAddr)
{
    struct sockaddr_in server;
    
    InetPton(AF_INET, serverAddr, &server.sin_addr.S_un.S_addr);
    //server.sin_addr.S_un.S_un_b.s_b1 = 1;
    //server.sin_addr.S_un.S_un_b.s_b1 = 0;
    //server.sin_addr.S_un.S_un_b.s_b1 = 0;
    //server.sin_addr.S_un.S_un_b.s_b1 = 127;

    server.sin_family = AF_INET;
    server.sin_port = htons(80);

    //Connect to remote server
    if (connect(s, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        puts("Socket connect error.");
        return FALSE;
    }

    puts("Socket connected");
    return TRUE;
}

////////////////////////////////////////////////////////////////////////////////

char server_reply[16384];
int recv_size, recv_tot;

VOID Recv_Print(VOID)
{
    recv_tot = 0;
    int doCount = 0;
    do {
        //Receive a reply from the server
        if ((recv_size = recv(sock, server_reply + recv_tot, 16384 - recv_tot, 0)) == SOCKET_ERROR)
        {
            puts("recv failed");
            exit(1);
        }
        if (++doCount > 10) {
            puts("server disconnected");
            exit(1);
        }
        //puts("Reply received\n");
        recv_tot += recv_size;
    } while (recv_tot < *(short *)server_reply);

    short packet_size = *(short *)server_reply;

    for (int i = 2; i < packet_size; i++) {
        putchar(server_reply[i]);
    }
    putchar('\n');
}

unsigned _stdcall ThreadFunction(LPVOID ptr)
{
    for (;;) {
        Recv_Print();
        Sleep(100);
    }
    return 0;
}

BOOL Tester(VOID)
{
loop:
    //Send some data
    int x = rand() % 100;
    int y = rand() % 100;

    printf("%d + %d = ", x, y);

    //message[0] = 6;
    //message[1] = 0;
    //message[2] = 0;
    //message[3] = 0;
    //message[4] = x;
    //message[5] = y;
    //message[6] = 0;

    size = EncodeNumbers((BYTE*)message, 0, x, y);
    *(SHORT *)message = size; // 이 역할은 서버에서 PutPacket이 한다

    if (send(sock, message, size, 0) < 0)    {
        puts("Send failed");
        return 1;
    }
    //    puts("Data Send\n");

    recv_tot = 0;
    int doCount = 0;
    do {
        //Receive a reply from the server
        if ((recv_size = recv(sock, server_reply + recv_tot, 16384 - recv_tot, 0)) == SOCKET_ERROR)
        {
            puts("recv failed");
            exit(1);
        }
        if (++doCount > 10) {
            puts("server disconnected");
            exit(1);
        }
        //puts("Reply received\n");
        recv_tot += recv_size;
    } while (recv_tot < *(short *)server_reply);

    int n = *(unsigned char *)(server_reply + 3);

    printf("%d, \t packetsize(%d)\n", n, recv_tot);
    if (x + y != n) {
        puts("summation error");
        exit(1);
    }

    goto loop;

    return TRUE;
}

VOID Room(VOID)
{
    size = EncodeRoom((BYTE*)message, ROOM);
    *(SHORT *)message = size; 
    send(sock, message, size, 0);

    //Recv_Print();
}

VOID User(VOID)
{
    size = EncodeUser((BYTE*)message, USER);
    *(SHORT *)message = size; 
    send(sock, message, size, 0);

    //Recv_Print();
}

VOID Enter(char *p)
{
    DWORD no;

    sscanf_s(p, "%d", &no);

    size = EncodeEnter((BYTE*)message, ENTER, no);
    *(SHORT *)message = size;
    send(sock, message, size, 0);

    //Recv_Print();
}

VOID Chat(char *p)
{
    size = EncodeChat((BYTE*)message, CHAT, p);

    *(SHORT *)message = size;
    send(sock, message, size, 0);

    //Recv_Print();
}