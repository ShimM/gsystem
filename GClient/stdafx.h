#pragma once

// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#include "targetver.h"

#include <stdio.h>
#include <tchar.h>
#include <assert.h>

// TODO: reference additional headers your program requires here

#include <winsock2.h>

#include "Stream.h"
#include "Protocol.h"

#define PORT    80
